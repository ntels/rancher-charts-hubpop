## Configuration

다음 표는 Memcached 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

|      Parameter            |          Description            |                         Default                         |
|---------------------------|---------------------------------|---------------------------------------------------------|
| `image`                   | The image to pull and run       | A recent official memcached tag                         |
| `imagePullPolicy`         | Image pull policy               | `Always` if `imageTag` is `latest`, else `IfNotPresent` |
| `memcached.verbosity`     | Verbosity level (v, vv, or vvv) | Un-set.                                                 |
| `memcached.maxItemMemory` | Max memory for items (in MB)    | `64`                                                    |
| `metrics.enabled`         | Expose metrics in prometheus format | false                                               |
| `metrics.image`           | The image to pull and run for the metrics exporter | A recent official memcached tag      |
| `metrics.imagePullPolicy` | Image pull policy               | `Always` if `imageTag` is `latest`, else `IfNotPresent` |
| `metrics.resources`       | CPU/Memory resource requests/limits for the metrics exporter | `{}`                       |
| `extraContainers`         | Container sidecar definition(s) as string | Un-set                                        |
| `extraVolumes`            | Volume definitions to add as string | Un-set                                              |

위의 파라미터는 `memcached` 파라미터에 매핑됩니다. 자세한 내용은 [Memcached documentation](https://github.com/memcached/memcached/wiki/ConfiguringServer)를 참조하십시오.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```bash
$ helm install --name my-release \
  --set memcached.verbosity=v \
    stable/memcached
```

위 명령은 Memcached 세부 정보를`v`로 설정합니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
$ helm install --name my-release -f values.yaml stable/memcached
```

> **Tip**: 기본값 [values.yaml](values.yaml)을 사용할 수 있습니다.
