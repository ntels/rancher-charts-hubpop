## 공식 문서

공식 프로젝트 문서 발견 [here](https://github.com/coreos/etcd-operator)

## 선행 조건

- Beta API가 활성화된 쿠버네티스 1.4 이상
- __Suggested:__ 백업 지원을 위한 기본 인프라의 PV 프로비저닝자 지원

## Chart 설치

릴리스 이름 `my-release`로 차트를 설치하려면 다음과 같이 하십시오:

```bash
$ helm install stable/etcd-operator --name my-release
```

__Note__: `cluster.enabled`를 설치하면 효과가 없다.
etcd 클러스터를 생성하기 전에 운영자가 TPR을 설치해야 하므로 헬름 설치 중에는 이 옵션이 무시되지만 업그레이드에는 사용할 수 있다.

## Chart 제거

`my-release` 배포를 제거/삭제하려면 다음과 같이 하십시오:

```bash
$ helm delete my-release
```

이 명령은 영구 볼륨을 제외한 모든 쿠버네티스 구성요소를 제거한다.

## 업데이트
TPR 리소스를 업데이트하면 TPR용 `kubectl apply`가 수정될 때까지 클러스터가 업데이트되지 않음([kubernetes/issues/29542](https://github.com/kubernetes/kubernetes/issues/29542) 참조)
옵션 주변의 작업들은 문서화된 [here](https://github.com/coreos/etcd-operator#resize-an-etcd-cluster)

## Configuration

다음 표에는 etcd-operator 차트의 구성 가능한 파라미터와 기본값이 나열되어 있다.

| Parameter                                         | Description                                                          | Default                                        |
| ------------------------------------------------- | -------------------------------------------------------------------- | ---------------------------------------------- |
| `rbac.create`                                     | install required RBAC service account, roles and rolebindings        | `true`                                         |
| `rbac.apiVersion`                                 | RBAC api version `v1alpha1|v1beta1`                                  | `v1beta1`                                      |
| `rbac.etcdOperatorServiceAccountName`             | Name of the service account resource when RBAC is enabled            | `etcd-operator-sa`                                      |
| `rbac.backupOperatorServiceAccountName`           | Name of the service account resource when RBAC is enabled            | `etcd-backup-operator-sa`                                      |
| `rbac.restoreOperatorServiceAccountName`          | Name of the service account resource when RBAC is enabled            | `etcd-restore-operator-sa`                                      |
| `deployments.etcdOperator`                        | Deploy the etcd cluster operator                                     | `true`                                         |
| `deployments.backupOperator`                      | Deploy the etcd backup operator                                      | `true`                                         |
| `deployments.restoreOperator`                     | Deploy the etcd restore operator                                     | `true`                                         |
| `customResources.createEtcdClusterCRD`            | Create a custom resource: EtcdCluster                                | `false`                                        |
| `customResources.createBackupCRD`                 | Create an a custom resource: EtcdBackup                              | `false`                                        |
| `customResources.createRestoreCRD`                | Create an a custom resource: EtcdRestore                             | `false`                                        |
| `etcdOperator.name`                               | Etcd Operator name                                                   | `etcd-operator`                                |
| `etcdOperator.replicaCount`                       | Number of operator replicas to create (only 1 is supported)          | `1`                                            |
| `etcdOperator.image.repository`                   | etcd-operator container image                                        | `quay.io/coreos/etcd-operator`                 |
| `etcdOperator.image.tag`                          | etcd-operator container image tag                                    | `v0.7.0`                                       |
| `etcdOperator.image.pullpolicy`                   | etcd-operator container image pull policy                            | `Always`                                       |
| `etcdOperator.resources.cpu`                      | CPU limit per etcd-operator pod                                      | `100m`                                         |
| `etcdOperator.resources.memory`                   | Memory limit per etcd-operator pod                                   | `128Mi`                                        |
| `etcdOperator.nodeSelector`                       | Node labels for etcd operator pod assignment                         | `{}`                                           |
| `etcdOperator.commandArgs`                        | Additional command arguments                                         | `{}`                                           |
| `backupOperator.name`                             | Backup operator name                                                 | `etcd-backup-operator`                         |
| `backupOperator.replicaCount`                     | Number of operator replicas to create (only 1 is supported)          | `1`                                            |
| `backupOperator.image.repository`                 | Operator container image                                             | `quay.io/coreos/etcd-operator`                 |
| `backupOperator.image.tag`                        | Operator container image tag                                         | `v0.7.0`                                       |
| `backupOperator.image.pullpolicy`                 | Operator container image pull policy                                 | `Always`                                       |
| `backupOperator.resources.cpu`                    | CPU limit per etcd-operator pod                                      | `100m`                                         |
| `backupOperator.resources.memory`                 | Memory limit per etcd-operator pod                                   | `128Mi`                                        |
| `backupOperator.spec.storageType`                 | Storage to use for backup file, currently only S3 supported          | `S3`                                           |
| `backupOperator.spec.s3.s3Bucket`                 | Bucket in S3 to store backup file                                    |                                                |
| `backupOperator.spec.s3.awsSecret`                | Name of kubernetes secret containing aws credentials                |                                                |
| `backupOperator.nodeSelector`                     | Node labels for etcd operator pod assignment                         | `{}`                                           |
| `backupOperator.commandArgs`                      | Additional command arguments                                         | `{}`                                           |
| `restoreOperator.name`                            | Restore operator name                                                | `etcd-backup-operator`                         |
| `restoreOperator.replicaCount`                    | Number of operator replicas to create (only 1 is supported)          | `1`                                            |
| `restoreOperator.image.repository`                | Operator container image                                             | `quay.io/coreos/etcd-operator`                 |
| `restoreOperator.image.tag`                       | Operator container image tag                                         | `v0.7.0`                                       |
| `restoreOperator.image.pullpolicy`                | Operator container image pull policy                                 | `Always`                                       |
| `restoreOperator.resources.cpu`                   | CPU limit per etcd-operator pod                                      | `100m`                                         |
| `restoreOperator.resources.memory`                | Memory limit per etcd-operator pod                                   | `128Mi`                                        |
| `restoreOperator.spec.s3.path`                    | Path in S3 bucket containing the backup file                         |                                                |
| `restoreOperator.spec.s3.awsSecret`               | Name of kubernetes secret containing aws credentials                |                                                |
| `restoreOperator.nodeSelector`                    | Node labels for etcd operator pod assignment                         | `{}`                                           |
| `restoreOperator.commandArgs`                     | Additional command arguments                                         | `{}`                                           |
| `etcdCluster.name`                                | etcd cluster name                                                    | `etcd-cluster`                                 |
| `etcdCluster.size`                                | etcd cluster size                                                    | `3`                                            |
| `etcdCluster.version`                             | etcd cluster version                                                 | `3.2.10`                                       |
| `etcdCluster.image.repository`                    | etcd container image                                                 | `quay.io/coreos/etcd-operator`                 |
| `etcdCluster.image.tag`                           | etcd container image tag                                             | `v3.2.10`                                      |
| `etcdCluster.image.pullPolicy`                    | etcd container image pull policy                                     | `Always`                                       |
| `etcdCluster.enableTLS`                           | Enable use of TLS                                                    | `false`                                        |
| `etcdCluster.tls.static.member.peerSecret`        | Kubernetes secret containing TLS peer certs                          | `etcd-peer-tls`                                |
| `etcdCluster.tls.static.member.serverSecret`      | Kubernetes secret containing TLS server certs                        | `etcd-server-tls`                              |
| `etcdCluster.tls.static.operatorSecret`           | Kubernetes secret containing TLS client certs                        | `etcd-client-tls`                              |
| `etcdCluster.pod.antiAffinity`                    | Whether etcd cluster pods should have an antiAffinity                | `false`                                        |
| `etcdCluster.pod.resources.limits.cpu`            | CPU limit per etcd cluster pod                                       | `100m`                                         |
| `etcdCluster.pod.resources.limits.memory`         | Memory limit per etcd cluster pod                                    | `128Mi`                                        |
| `etcdCluster.pod.resources.requests.cpu`          | CPU request per etcd cluster pod                                     | `100m`                                         |
| `etcdCluster.pod.resources.requests.memory`       | Memory request per etcd cluster pod                                  | `128Mi`                                        |
| `etcdCluster.pod.nodeSelector`                    | node labels for etcd cluster pod assignment                          | `{}`                                           |

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면:

```bash
$ helm install --name my-release --set image.tag=v0.2.1 stable/etcd-operator
```

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면:

```bash
$ helm install --name my-release --values values.yaml stable/etcd-operator
```

## RBAC
기본적으로 차트는 권장 RBAC 역할과 역할 바인딩을 설치한다.

클러스터에서 이 실행을 지원하는지 확인하려면 다음과 같이 하십시오:

```console
$ kubectl api-versions | grep rbac
```

또한 api 서버에 다음 매개 변수를 가져야 한다. 사용 방법은 다음 문서를 참조하십시오 [RBAC](https://kubernetes.io/docs/admin/authorization/rbac/). 

```
--authorization-mode=RBAC
```

출력에 "베타" 또는 "알파"와 "베타"가 모두 포함된 경우, 기본적으로 rbac을 설치할 수 있으며, 그렇지 않은 경우 아래 설명된 대로 RBAC를 끌 수 있다.

### RBAC role/rolebinding 생성

RBAC 자원은 기본적으로 활성화된다. RBAC를 비활성화하려면 다음을 수행하십시오:

```console
$ helm install --name my-release stable/etcd-operator --set rbac.create=false
```

### RBAC 매니페스트 apiVersion 변경

기본적으로 RBAC 리소스는 "v1beta1" apiVersion으로 생성된다. "v1alpha1"을 사용하려면 다음을 수행하십시오.

```console
$ helm install --name my-release stable/etcd-operator --set rbac.install=true,rbac.apiVersion=v1alpha1
```
