## Configuration

Parameter | Description | Default
--- | --- | ---
`routePrefix` | Prefix used to register routes | `"/"`
`auth.anonymous.enabled` | If true, enable anonymous authentication | `true`
`adminUser` | Grafana admin user name | `admin`
`adminPassword` | Grafana admin user password | `admin`
`image.repository` | Image | `grafana/grafana`
`image.tag` | Image tag | `4.4.1`
`extraVars` | Pass extra environment variables to the Grafana container. | `{}`
`grafanaWatcher.repository` | Image | `quay.io/coreos/grafana-watcher`
`grafanaWatcher.tag` | Image tag | `v0.0.8`
`ingress.enabled` | If true, Grafana Ingress will be created | `false`
`ingress.annotations` | Annotations for Grafana Ingress | `{}`
`ingress.labels` | Labels for Grafana Ingress | `{}`
`ingress.hosts` | Grafana Ingress fully-qualified domain names | `[]`
`ingress.tls` | TLS configuration for Grafana Ingress | `[]`
`nodeSelector` | Node labels for pod assignment | `{}`
`resources` | Pod resource requests & limits | `{}`
`service.annotations` | Annotations to be added to the Grafana Service | `{}`
`service.clusterIP` | Cluster-internal IP address for Grafana Service | `""`
`service.externalIPs` | List of external IP addresses at which the Grafana Service will be available | `[]`
`service.loadBalancerIP` | External IP address to assign to Grafana Service | `""`
`service.loadBalancerSourceRanges` | List of client IPs allowed to access Grafana Service | `[]`
`service.nodePort` | Port to expose Grafana Service on each node | `30902`
`service.type` | Grafana Service type | `ClusterIP`
`storageSpec` | Grafana StorageSpec for persistent data | `{}`

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오.
```

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다.

> **Tip**: 기본값 [values.yaml](values.yaml)을 사용할 수 있다.

> **Tip**: GCE에서 `Ingress.enabled=true`를 사용하려면 `service.type=NodePort`를 넣어야 한다.