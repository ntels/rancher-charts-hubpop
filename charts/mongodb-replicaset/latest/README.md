## 구성

다음 표는 mongodb 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

| Parameter                           | Description                                                               | Default                                             |
| ----------------------------------- | ------------------------------------------------------------------------- | --------------------------------------------------- |
| `replicas`                          | Number of replicas in the replica set                                     | `3`                                                 |
| `replicaSetName`                    | The name of the replica set                                               | `rs0`                                               |
| `podDisruptionBudget`               | Pod disruption budget                                                     | `{}`                                                |
| `port`                              | MongoDB port                                                              | `27017`                                             |
| `installImage.repository`           | Image name for the install container                                      | `k8s.gcr.io/mongodb-install`                        |
| `installImage.tag`                  | Image tag for the install container                                       | `0.5`                                               |
| `installImage.pullPolicy`           | Image pull policy for the init container that establishes the replica set | `IfNotPresent`                                      |
| `image.repository`                  | MongoDB image name                                                        | `mongo`                                             |
| `image.tag`                         | MongoDB image tag                                                         | `3.6`                                               |
| `image.pullPolicy`                  | MongoDB image pull policy                                                 | `IfNotPresent`                                      |
| `podAnnotations`                    | Annotations to be added to MongoDB pods                                   | `{}`                                                |
| `securityContext`                   | Security context for the pod                                              | `{runAsUser: 999, fsGroup: 999, runAsNonRoot: true}`|
| `resources`                         | Pod resource requests and limits                                          | `{}`                                                |
| `persistentVolume.enabled`          | If `true`, persistent volume claims are created                           | `true`                                              |
| `persistentVolume.storageClass`     | Persistent volume storage class                                           | ``                                                  |
| `persistentVolume.accessMode`       | Persistent volume access modes                                            | `[ReadWriteOnce]`                                   |
| `persistentVolume.size`             | Persistent volume size                                                    | `10Gi`                                              |
| `persistentVolume.annotations`      | Persistent volume annotations                                             | `{}`                                                |
| `tls.enabled`                       | Enable MongoDB TLS support including authentication                       | `false`                                             |
| `tls.cacert`                        | The CA certificate used for the members                                   | Our self signed CA certificate                      |
| `tls.cakey`                         | The CA key used for the members                                           | Our key for the self signed CA certificate          |
| `auth.enabled`                      | If `true`, keyfile access control is enabled                              | `false`                                             |
| `auth.key`                          | Key for internal authentication                                           | ``                                                  |
| `auth.existingKeySecret`            | If set, an existing secret with this name for the key is used             | ``                                                  |
| `auth.adminUser`                    | MongoDB admin user                                                        | ``                                                  |
| `auth.adminPassword`                | MongoDB admin password                                                    | ``                                                  |
| `auth.existingAdminSecret`          | If set, and existing secret with this name is used for the admin user     | ``                                                  |
| `serviceAnnotations`                | Annotations to be added to the service                                    | `{}`                                                |
| `configmap`                         | Content of the MongoDB config file                                        | ``                                                  |
| `nodeSelector`                      | Node labels for pod assignment                                            | `{}`                                                |
| `affinity`                          | Node/pod affinities                                                       | `{}`                                                |
| `tolerations`                       | List of node taints to tolerate                                           | `[]`                                                |
| `livenessProbe`                     | Liveness probe configuration                                              | See below                                           |
| `readinessProbe`                    | Readiness probe configuration                                             | See below                                           |
| `extraVars`                         | Set environment variables for the main container                          | `{}`                                                |

*MongoDB 설정 파일*

차트 구성에 의존하는 모든 옵션은`mongod`에 명령 행 인수로 제공됩니다. 기본적으로, 차트는 빈 구성 파일을 만듭니다. `configmap` 구성 값을 통해 항목을 추가 할 수 있습니다.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```console
$ helm install --name my-release -f values.yaml stable/mongodb-replicaset
```

> **Tip**: 기본 [values.yaml] (values.yaml)을 사용할 수 있습니다.

3 개의 노드가 모두 실행 중이면이 디렉토리에서 "test.sh"스크립트를 실행할 수 있습니다.이 스크립트는 기본에 키를 삽입하고 보조에 대한 출력을 점검합니다. 이 스크립트는 포드에 액세스하려면`$ RELEASE_NAME` 환경 변수를 설정해야합니다.

## 입증

기본적으로이 차트는 인증없이 MongoDB 복제 세트를 작성합니다. 파라미터 `auth.enabled`를 사용하여 인증을 활성화 할 수 있습니다. 활성화되면 키 파일 액세스 제어가 설정되고
루트 권한을 가진 관리자가 생성됩니다. 사용자 자격 증명 및 키 파일을 직접 지정할 수 있습니다.
대안 적으로, 기존의 비밀이 제공 될 수있다. 관리자의 비밀은 `user` 및 `password` 키를 포함해야하며, 키 파일의 경우 `key.txt`가 포함되어야합니다. 사용자는 완전한 `root` 권한으로 작성되지만 보안을 위해 `admin` 데이터베이스로 제한됩니다. 보다 구체적인 권한을 가진 추가 사용자를 만드는 데 사용할 수 있습니다.

## TLS 지원

전체 TLS 암호화를 활성화하려면`tls.enabled`를`true`로 설정하십시오. 다음을 실행하여 자체 CA를 작성하는 것이 좋습니다:

```console
$ openssl genrsa -out ca.key 2048
$ openssl req -x509 -new -nodes -key ca.key -days 10000 -out ca.crt -subj "/CN=mydomain.com"
```

그런 다음 base64로 인코딩 된 (`cat ca.key | base64 -w0`) 인증서와 `tls.cacert` 및 `tls.cakey` 필드에 키를 붙여 넣습니다. 다음과 같이 복제 세트의 구성 맵을 조정하십시오:

```yml
configmap:
  storage:
    dbPath: /data/db
  net:
    port: 27017
    ssl:
      mode: requireSSL
      CAFile: /ca/tls.crt
      PEMKeyFile: /work-dir/mongo.pem
  replication:
    replSetName: rs0
  security:
    authorization: enabled
    clusterAuthMode: x509
    keyFile: /keydir/key.txt
```

클러스터에 액세스하려면 클러스터 설정 중 `/work-dir/mongo.pem`에 클러스터 설정 중에 생성 된 인증서 중 하나가 필요합니다.
특정 컨테이너 또는 다음을 통해 자신의 컨테이너를 생성합니다.

```console
$ cat >openssl.cnf <<EOL
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $HOSTNAME1
DNS.1 = $HOSTNAME2
EOL
$ openssl genrsa -out mongo.key 2048
$ openssl req -new -key mongo.key -out mongo.csr -subj "/CN=$HOSTNAME" -config openssl.cnf
$ openssl x509 -req -in mongo.csr \
    -CA $MONGOCACRT -CAkey $MONGOCAKEY -CAcreateserial \
    -out mongo.crt -days 3650 -extensions v3_req -extfile openssl.cnf
$ rm mongo.csr
$ cat mongo.crt mongo.key > mongo.pem
$ rm mongo.key mongo.crt
```

`$HOSTNAME`을 실제 호스트 이름으로 바꾸고 `$HOSTNAME1`, `$HOSTNAME2` 등을 MongoDB 복제 세트에 대한 액세스를 허용하려는 대체 호스트 이름으로 교환하십시오. 이제 `mongo.pem` 인증서를 사용하여 mongodb에 인증 할 수 있습니다:

```console
$ mongo --ssl --sslCAFile=ca.crt --sslPEMKeyFile=mongo.pem --eval "db.adminCommand('ping')"
```
## Readiness 프로브
Readiness 프로브의 기본값은:

```yaml
readinessProbe:
  initialDelaySeconds: 5
  timeoutSeconds: 1
  failureThreshold: 3
  periodSeconds: 10
  successThreshold: 1
```

## Liveness 프로브
liveness 프로브의 기본값은:

```yaml
livenessProbe:
  initialDelaySeconds: 30
  timeoutSeconds: 5
  failureThreshold: 3
  periodSeconds: 10
  successThreshold: 1
```

## Deep dive

포드 이름은 선택된 이름에 종속되므로 다음 예제에서는 환경 변수 `RELEASENAME`을 사용합니다. 예를 들어, 헬름 릴리스 이름이 `messy-hydra`인 경우 계속하기 전에 다음을 설정해야합니다. 아래 예제 스크립트는 3 개의 포드 만 가정합니다.

```console
export RELEASE_NAME=messy-hydra
```

### Cluster Health

```console
$ for i in 0 1 2; do kubectl exec $RELEASE_NAME-mongodb-replicaset-$i -- sh -c 'mongo --eval="printjson(db.serverStatus())"'; done
```

### Failover

다음을 사용하여 각 노드가 수행하는 역할을 확인할 수 있습니다:
```console
$ for i in 0 1 2; do kubectl exec $RELEASE_NAME-mongodb-replicaset-$i -- sh -c 'mongo --eval="printjson(rs.isMaster())"'; done

MongoDB shell version: 3.6.3
connecting to: mongodb://127.0.0.1:27017
MongoDB server version: 3.6.3
{
	"hosts" : [
		"messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
		"messy-hydra-mongodb-1.messy-hydra-mongodb.default.svc.cluster.local:27017",
		"messy-hydra-mongodb-2.messy-hydra-mongodb.default.svc.cluster.local:27017"
	],
	"setName" : "rs0",
	"setVersion" : 3,
	"ismaster" : true,
	"secondary" : false,
	"primary" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
	"me" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
	"electionId" : ObjectId("7fffffff0000000000000001"),
	"maxBsonObjectSize" : 16777216,
	"maxMessageSizeBytes" : 48000000,
	"maxWriteBatchSize" : 1000,
	"localTime" : ISODate("2016-09-13T01:10:12.680Z"),
	"maxWireVersion" : 4,
	"minWireVersion" : 0,
	"ok" : 1
}


```
이를 통해 어떤 멤버가 기본 멤버인지 확인할 수 있습니다.

이제 지속성 및 장애 조치를 테스트하겠습니다. 먼저 키를 삽입합니다(아래 예에서는 포드 0이 마스터라고 가정):
```console
$ kubectl exec $RELEASE_NAME-mongodb-replicaset-0 -- mongo --eval="printjson(db.test.insert({key1: 'value1'}))"

MongoDB shell version: 3.6.3
connecting to: mongodb://127.0.0.1:27017
{ "nInserted" : 1 }
```

기존 멤버 조회:
```console
$ kubectl run --attach bbox --image=mongo:3.6 --restart=Never --env="RELEASE_NAME=$RELEASE_NAME" -- sh -c 'while true; do for i in 0 1 2; do echo $RELEASE_NAME-mongodb-replicaset-$i $(mongo --host=$RELEASE_NAME-mongodb-replicaset-$i.$RELEASE_NAME-mongodb-replicaset --eval="printjson(rs.isMaster())" | grep primary); sleep 1; done; done';

Waiting for pod default/bbox2 to be running, status is Pending, pod ready: false
If you don't see a command prompt, try pressing enter.
messy-hydra-mongodb-2 "primary" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
messy-hydra-mongodb-0 "primary" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
messy-hydra-mongodb-1 "primary" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
messy-hydra-mongodb-2 "primary" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
messy-hydra-mongodb-0 "primary" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",

```

Primary을 죽이고 새 마스터으로 선출되는 것을 지켜보십시오.
```console
$ kubectl delete pod $RELEASE_NAME-mongodb-replicaset-0

pod "messy-hydra-mongodb-0" deleted
```

모든 포드를 삭제하고 statefulset 컨트롤러가 가져 오도록합니다.
```console
$ kubectl delete po -l "app=mongodb-replicaset,release=$RELEASE_NAME"
$ kubectl get po --watch-only
NAME                    READY     STATUS        RESTARTS   AGE
messy-hydra-mongodb-0   0/1       Pending   0         0s
messy-hydra-mongodb-0   0/1       Pending   0         0s
messy-hydra-mongodb-0   0/1       Pending   0         7s
messy-hydra-mongodb-0   0/1       Init:0/2   0         7s
messy-hydra-mongodb-0   0/1       Init:1/2   0         27s
messy-hydra-mongodb-0   0/1       Init:1/2   0         28s
messy-hydra-mongodb-0   0/1       PodInitializing   0         31s
messy-hydra-mongodb-0   0/1       Running   0         32s
messy-hydra-mongodb-0   1/1       Running   0         37s
messy-hydra-mongodb-1   0/1       Pending   0         0s
messy-hydra-mongodb-1   0/1       Pending   0         0s
messy-hydra-mongodb-1   0/1       Init:0/2   0         0s
messy-hydra-mongodb-1   0/1       Init:1/2   0         20s
messy-hydra-mongodb-1   0/1       Init:1/2   0         21s
messy-hydra-mongodb-1   0/1       PodInitializing   0         24s
messy-hydra-mongodb-1   0/1       Running   0         25s
messy-hydra-mongodb-1   1/1       Running   0         30s
messy-hydra-mongodb-2   0/1       Pending   0         0s
messy-hydra-mongodb-2   0/1       Pending   0         0s
messy-hydra-mongodb-2   0/1       Init:0/2   0         0s
messy-hydra-mongodb-2   0/1       Init:1/2   0         21s
messy-hydra-mongodb-2   0/1       Init:1/2   0         22s
messy-hydra-mongodb-2   0/1       PodInitializing   0         25s
messy-hydra-mongodb-2   0/1       Running   0         26s
messy-hydra-mongodb-2   1/1       Running   0         30s


...
messy-hydra-mongodb-0 "primary" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
messy-hydra-mongodb-1 "primary" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
messy-hydra-mongodb-2 "primary" : "messy-hydra-mongodb-0.messy-hydra-mongodb.default.svc.cluster.local:27017",
```

이전에 삽입 한 키 확인:
```console
$ kubectl exec $RELEASE_NAME-mongodb-replicaset-1 -- mongo --eval="rs.slaveOk(); db.test.find({key1:{\$exists:true}}).forEach(printjson)"

MongoDB shell version: 3.6.3
connecting to: mongodb://127.0.0.1:27017
{ "_id" : ObjectId("57b180b1a7311d08f2bfb617"), "key1" : "value1" }
```

### 스케일링

스케일링은 권장되는 `helm upgrade`에 의해 관리되어야합니다.
