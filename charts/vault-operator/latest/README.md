## ## 개요
Vault 운영자는 Kubernetes에서 [Vault][vault] 클러스터를 배포하고 관리합니다. Vault 운영자가 생성 한 Vault 인스턴스는 가용성이 높으며 자동 장애 조치 및 업그레이드를 지원합니다.

### 프로젝트 상태 : 베타
기본 기능이 완료되었으며 현재 주요 API 변경이 계획되지 않았지만 프로젝트가 안정적으로 선언되기 전에 API가 호환되지 않는 방식으로 변경 될 수 있습니다.

## 구성
Parameter | Description | Default
--------- | ----------- | -------
`rbac.create` | If true, create & use RBAC resources | `true`
`serviceAccounts.create` | If true, create the values-operator service account | `true`
`imagePullPolicy` | all containers image pull policy | `IfNotPresent`
`vaultOperator.replicaCount` | desired number of vault operator controller pod | `1`
`vaultOperator.image.repository` | vault operator container image repository | `quay.io/coreos/vault-operator`
`vaultOperator.image.tag` | vault operator container image tag | `latest`
`vaultOperator.resources` | vault operator pod resource requests & limits | `{}`
`vaultOperator.nodeSelector` | node labels for vault operator pod assignment | `{}`
`vault.node` | desired number of vault cluster nodes | `2`
`vault.version` | vault app version | `0.9.1-0`
`etcd.image.repository` | etcd container image repository | `quay.io/coreos/etcd-operator`
`etcd.image.tag` | etcd container image tag | `v0.8.3`
`ui.replicaCount` | desired number of Vault UI pod | `1`
`ui.image.repository` | Vault UI container image repository | `djenriquez/vault-ui`
`ui.image.tag` | Vault UI container image tag | `latest`
`ui.resources` | Vault UI pod resource requests & limits | `{}`
`ui.nodeSelector` | node labels for Vault UI pod assignment | `{}`
`ui.ingress.enabled` | If true, Vault UI Ingress will be created | `false`
`ui.ingress.annotations` | Vault UI Ingress annotations | `{}`
`ui.ingress.hosts` | Vault UI Ingress hostnames | `[]`
`ui.ingress.tls` | Vault UI Ingress TLS configuration (YAML) | `[]`
`ui.vault.auth` | Vault UI login method | `TOKEN`
`ui.service.name` | Vault UI service name | `vault-ui`
`ui.service.type` | type of ui service to create | `ClusterIP`
`ui.service.externalPort` | Vault UI service target port | `8000`
`ui.service.internalPort` | Vault UI container port | `8000`
`ui.service.nodePort` | Port to be used as the service NodePort (ignored if `server.service.type` is not `NodePort`) | `0`


## Vault 클러스터 사용

배포 된 Vault 클러스터를 초기화, 개봉 및 사용하는 방법에 대해서는 [Vault usage guide](https://github.com/coreos/vault-operator/blob/master/doc/user/vault.md)를 참조하십시오.

Prometheus를 사용하여 Vault 클러스터를 모니터링하고 경고하는 방법은 [monitoring guide](https://github.com/coreos/vault-operator/blob/master/doc/user/monitoring.md)를 참조하십시오.

etcd opeartor를 사용하여 Vault 클러스터 데이터를 백업 및 복원하는 방법은 [recovery guide](https://github.com/coreos/vault-operator/blob/master/doc/user/recovery.md)를 참조하십시오.

기본 TLS 구성에 대한 개요 또는 Vault 클러스터에 대한 사용자 정의 TLS 자산을 지정하는 방법은 [TLS setup guide](https://github.com/coreos/vault-operator/blob/master/doc/user/tls_setup.md)를 참조하십시오.

[vault]: https://www.vaultproject.io/
[etcd-operator]: https://github.com/coreos/etcd-operator/
