# phpBB

[phpBB](https://www.phpbb.com/)는 PHP 스크립팅 언어로 작성된 인터넷 포럼 패키지입니다. "phpBB"라는 이름은 PHP 게시판의 약자입니다.

## 소개

이 차트는 [Helm](https://helm.sh) 패키지 관리자를 사용하여 [Kubernetes](http://kubernetes.io) 클러스터에서 [phpBB](https://github.com/bitnami/bitnami-docker-phpbb) 배포를 부트 스트랩합니다.

또한 phpBB 애플리케이션의 데이터베이스 요구 사항을 위해 MariaDB 배치를 부트 스트랩하는 데 필요한 [Bitnami MariaDB chart](https://github.com/kubernetes/charts/tree/master/stable/mariadb)을 패키지화합니다.

## 구성

다음 표는 phpBB 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

|             Parameter             |              Description              |                         Default                         |
|-----------------------------------|---------------------------------------|---------------------------------------------------------|
| `image.registry`                  | phpBB image registry                  | `docker.io`                                             |
| `image.repository`                | phpBB image name                      | `bitnami/phpbb`                                         |
| `image.tag`                       | phpBB image tag                       | `{VERSION}`                                             |
| `image.pullPolicy`                | Image pull policy                     | `Always` if `imageTag` is `latest`, else `IfNotPresent` |
| `image.pullSecrets`               | Specify image pull secrets            | `nil`                                                   |
| `phpbbUser`                       | User of the application               | `user`                                                  |
| `phpbbPassword`                   | Application password                  | _random 10 character long alphanumeric string_          |
| `phpbbEmail`                      | Admin email                           | `user@example.com`                                      |
| `allowEmptyPassword`              | Allow DB blank passwords              | `yes`                                                   |
| `smtpHost`                        | SMTP host                             | `nil`                                                   |
| `smtpPort`                        | SMTP port                             | `nil`                                                   |
| `smtpUser`                        | SMTP user                             | `nil`                                                   |
| `smtpPassword`                    | SMTP password                         | `nil`                                                   |
| `externalDatabase.host`           | Host of the external database         | `nil`                                                   |
| `externalDatabase.user`           | Existing username in the external db  | `bn_phpbb`                                              |
| `externalDatabase.password`       | Password for the above username       | `nil`                                                   |
| `externalDatabase.database`       | Name of the existing database         | `bitnami_phpbb`                                         |
| `mariadb.enabled`                 | Use or not the MariaDB chart          | `true`                                                  |
| `mariadb.rootUser.password`     | MariaDB admin password                | `nil`                                                   |
| `mariadb.db.name`         | Database name to create               | `bitnami_phpbb`                                         |
| `mariadb.db.user`             | Database user to create               | `bn_phpbb`                                              |
| `mariadb.db.password`         | Password for the database             | _random 10 character long alphanumeric string_          |
| `serviceType`                     | Kubernetes Service type               | `LoadBalancer`                                          |
| `persistence.enabled`             | Enable persistence using PVC          | `true`                                                  |
| `persistence.apache.storageClass` | PVC Storage Class for Apache volume   | `nil` (uses alpha storage class annotation)             |
| `persistence.apache.accessMode`   | PVC Access Mode for Apache volume     | `ReadWriteOnce`                                         |
| `persistence.apache.size`         | PVC Storage Request for Apache volume | `1Gi`                                                   |
| `persistence.phpbb.storageClass`  | PVC Storage Class for phpBB volume    | `nil` (uses alpha storage class annotation)             |
| `persistence.phpbb.accessMode`    | PVC Access Mode for phpBB volume      | `ReadWriteOnce`                                         |
| `persistence.phpbb.size`          | PVC Storage Request for phpBB volume  | `8Gi`                                                   |
| `resources`                       | CPU/Memory resource requests/limits   | Memory: `512Mi`, CPU: `300m`                            |

위의 매개 변수는 [bitnami/phpbb](http://github.com/bitnami/bitnami-docker-phpbb)에 정의 된 env 변수에 매핑됩니다. 자세한 내용은 [bitnami/phpbb](http://github.com/bitnami/bitnami-docker-phpbb) 이미지 설명서를 참조하십시오.

## 영구적

[Bitnami phpBB](https://github.com/bitnami/bitnami-docker-phpbb) 이미지는 컨테이너의 `/bitnami/phpbb` 및 `/bitnami/apache` 경로에 phpBB 데이터 및 구성을 저장합니다.

영구 볼륨 클레임은 배포 전반에 걸쳐 데이터를 유지하는 데 사용됩니다. 이것은 GCE, AWS 및 minikube에서 작동하는 것으로 알려져 있습니다.
PVC를 구성하거나 지속성을 비활성화하려면 [Configuration](#configuration) 섹션을 참조하십시오.