## 구성

다음 표는 Redis 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

| Parameter                                  | Description                                                                                                    | Default                              |
|--------------------------------------------|----------------------------------------------------------------------------------------------------------------|--------------------------------------|
| `image.registry`                           | Redis Image registry                                                                                           | `docker.io`                                          |
| `image.repository`                         | Redis Image name                                                                                               | `bitnami/redis`                                      |
| `image.tag`                                | Redis Image tag                                                                                                | `{VERSION}`                                          |
| `image.pullPolicy`                         | Image pull policy                                                                                              | `Always`                                             |
| `image.pullSecrets`                        | Specify docker-ragistry secret names as an array                                                               | `nil`                                                |
| `cluster.enabled`                          | Use master-slave topology                                                                                      | `true`                                               |
| `cluster.slaveCount`                       | Number of slaves                                                                                               | 1                                                    |
| `existingSecret`                           | Name of existing secret object (for password authentication)                                                   | `nil`                                                |
| `usePassword`                              | Use password                                                                                                   | `true`                                               |
| `password`                                 | Redis password (ignored if existingSecret set)                                                                 | Randomly generated                                   |
| `networkPolicy.enabled`                    | Enable NetworkPolicy                                                                                           | `false`                                              |
| `networkPolicy.allowExternal`              | Don't require client label for connections                                                                     | `true`                                               |
| `metrics.enabled`                          | Start a side-car prometheus exporter                                                                           | `false`                                              |
| `metrics.image.registry`                   | Redis Image registry                                                                                           | `docker.io`                                          |
| `metrics.image.repository`                 | Redis Image name                                                                                               | `bitnami/redis`                                      |
| `metrics.image.tag`                        | Redis Image tag                                                                                                | `{VERSION}`                                          |
| `metrics.image.pullPolicy`                 | Image pull policy                                                                                              | `IfNotPresent`                                       |
| `metrics.image.pullSecrets`                | Specify docker-registry secret names as an array                                                               | `nil`                                                |
| `metrics.podLabels`                        | Additional labels for Metrics exporter pod                                                                     | {}                                                   |
| `metrics.podAnnotations`                   | Additional annotations for Metrics exporter pod                                                                | {}                                                   |
| `metrics.targetServiceAnnotations`         | Annotations for the services to monitor  (redis master and redis slave service)                                | {}                                                   |
| `metrics.resources`                        | Exporter resource requests/limit                                                                               | Memory: `256Mi`, CPU: `100m`                         |
| `master.persistence.enabled`               | Use a PVC to persist data (master node)                                                                        | `true`                                               |
| `master.persistence.path`                  | Path to mount the volume at, to use other images                                                               | `/bitnami`                                           |
| `master.persistence.subPath`               | Subdirectory of the volume to mount at                                                                         | `""`                                                 |
| `master.persistence.storageClass`          | Storage class of backing PVC                                                                                   | `generic`                                            |
| `master.persistence.accessModes`           | Persistent Volume Access Modes                                                                                 | `[ReadWriteOnce]`                                    |
| `master.persistence.size`                  | Size of data volume                                                                                            | `8Gi`                                                |
| `master.podLabels`                         | Additional labels for Redis master pod                                                                         | {}                                                   |
| `master.podAnnotations`                    | Additional annotations for Redis master pod                                                                    | {}                                                   |
| `master.port`                              | Redis master port                                                                                              | 6379                                                 |
| `master.args`                              | Redis master command-line args                                                                                 | []                                                   |
| `master.disableCommands`                   | Comma-separated list of Redis commands to disable (master)                                                     | `FLUSHDB,FLUSHALL`                                   |
| `master.extraFlags`                        | Redis master additional command line flags                                                                     | []                                                   |
| `master.nodeSelector`                      | Redis master Node labels for pod assignment                                                                    | {"beta.kubernetes.io/arch": "amd64"}                 |
| `master.tolerations`                       | Toleration labels for Redis master pod assignment                                                              | []                                                   |
| `master.service.type`                      | Kubernetes Service type (redis master)                                                                         | `LoadBalancer`                                       |
| `master.service.annotations`               | annotations for redis master service                                                                           | {}                                                   |
| `master.service.loadBalancerIP`            | loadBalancerIP if redis master service type is `LoadBalancer`                                                  | `nil`                                                |
| `master.securityContext.enabled`           | Enable security context (redis master pod)                                                                     | `true`                                               |
| `master.securityContext.fsGroup`           | Group ID for the container (redis master pod)                                                                  | `1001`                                               |
| `master.securityContext.runAsUser`         | User ID for the container (redis master pod)                                                                   | `1001`                                               |
| `master.resources`                         | Redis master CPU/Memory resource requests/limits                                                               | Memory: `256Mi`, CPU: `100m`                         |
| `master.livenessProbe.enabled`             | Turn on and off liveness probe (redis master pod)                                                              | `true`                                               |
| `master.livenessProbe.initialDelaySeconds` | Delay before liveness probe is initiated (redis master pod)                                                    | `30`                                                 |
| `master.livenessProbe.periodSeconds`       | How often to perform the probe (redis master pod)                                                              | `30`                                                 |
| `master.livenessProbe.timeoutSeconds`      | When the probe times out (redis master pod)                                                                    | `5`                                                  |
| `master.livenessProbe.successThreshold`    | Minimum consecutive successes for the probe to be considered successful after having failed (redis master pod) | `1`                                                  |
| `master.livenessProbe.failureThreshold`    | Minimum consecutive failures for the probe to be considered failed after having succeeded.                     | `5`                                                  |
| `master.readinessProbe.enabled`            | Turn on and off readiness probe (redis master pod)                                                             | `true`                                               |
| `master.readinessProbe.initialDelaySeconds`| Delay before readiness probe is initiated (redis master pod)                                                   | `5`                                                  |
| `master.readinessProbe.periodSeconds`      | How often to perform the probe (redis master pod)                                                              | `10`                                                 |
| `master.readinessProbe.timeoutSeconds`     | When the probe times out (redis master pod)                                                                    | `1`                                                  |
| `master.readinessProbe.successThreshold`   | Minimum consecutive successes for the probe to be considered successful after having failed (redis master pod) | `1`                                                  |
| `master.readinessProbe.failureThreshold`   | Minimum consecutive failures for the probe to be considered failed after having succeeded.                     | `5`                                                  |
| `slave.serviceType`                        | Kubernetes Service type (redis slave)                                                                          | `LoadBalancer`                                       |
| `slave.service.annotations`                | annotations for redis slave service                                                                            | {}                                                   |
| `slave.service.loadBalancerIP`             | LoadBalancerIP if Redis slave service type is `LoadBalancer`                                                   | `nil`                                                |
| `slave.port`                               | Redis slave port                                                                                               | `master.port`                                        |
| `slave.args`                               | Redis slave command-line args                                                                                  | `master.args`                                        |
| `slave.disableCommands`                    | Comma-separated list of Redis commands to disable (slave)                                                      | `master.disableCommands`                             |
| `slave.extraFlags`                         | Redis slave additional command line flags                                                                      | `master.extraFlags`                                  |
| `slave.livenessProbe.enabled`              | Turn on and off liveness probe (redis slave pod)                                                               | `master.livenessProbe.enabled`                       |
| `slave.livenessProbe.initialDelaySeconds`  | Delay before liveness probe is initiated (redis slave pod)                                                     | `master.livenessProbe.initialDelaySeconds`           |
| `slave.livenessProbe.periodSeconds`        | How often to perform the probe (redis slave pod)                                                               | `master.livenessProbe.periodSeconds`                 |
| `slave.livenessProbe.timeoutSeconds`       | When the probe times out (redis slave pod)                                                                     | `master.livenessProbe.timeoutSeconds`                |
| `slave.livenessProbe.successThreshold`     | Minimum consecutive successes for the probe to be considered successful after having failed (redis slave pod)  | `master.livenessProbe.successThreshold`              |
| `slave.livenessProbe.failureThreshold`     | Minimum consecutive failures for the probe to be considered failed after having succeeded.                     | `master.livenessProbe.failureThreshold`              |
| `slave.readinessProbe.enabled`             | Turn on and off slave.readiness probe (redis slave pod)                                                        | `master.readinessProbe.enabled`                      |
| `slave.readinessProbe.initialDelaySeconds` | Delay before slave.readiness probe is initiated (redis slave pod)                                              | `master.readinessProbe.initialDelaySeconds`          |
| `slave.readinessProbe.periodSeconds`       | How often to perform the probe (redis slave pod)                                                               | `master.readinessProbe.periodSeconds`                |
| `slave.readinessProbe.timeoutSeconds`      | When the probe times out (redis slave pod)                                                                     | `master.readinessProbe.timeoutSeconds`               |
| `slave.readinessProbe.successThreshold`    | Minimum consecutive successes for the probe to be considered successful after having failed (redis slave pod)  | `master.readinessProbe.successThreshold`             |
| `slave.readinessProbe.failureThreshold`    | Minimum consecutive failures for the probe to be considered failed after having succeeded. (redis slave pod)   | `master.readinessProbe.failureThreshold`             |
| `slave.podLabels`                          | Additional labels for Redis slave pod                                                                          | `master.podLabels`                                   |
| `slave.podAnnotations`                     | Additional annotations for Redis slave pod                                                                     | `master.podAnnotations`                              |
| `slave.securityContext.enabled`            | Enable security context (redis slave pod)                                                                      | `master.securityContext.enabled`                     |
| `slave.securityContext.fsGroup`            | Group ID for the container (redis slave pod)                                                                   | `master.securityContext.fsGroup`                     |
| `slave.securityContext.runAsUser`          | User ID for the container (redis slave pod)                                                                    | `master.securityContext.runAsUser`                   |
| `slave.resources`                          | Redis slave CPU/Memory resource requests/limits                                                                | `master.resources`                                   |

위의 파라미터는 [bitnami/redis](http://github.com/bitnami/bitnami-docker-redis)에 정의 된 env 변수에 매핑됩니다. 자세한 내용은 [bitnami/redis](http://github.com/bitnami/bitnami-docker-redis) 이미지 설명서를 참조하십시오.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```bash
$ helm install --name my-release \
  --set password=secretpassword \
    stable/redis
```

위 명령은 Redis 서버 비밀번호를`secretpassword`로 설정합니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
$ helm install --name my-release -f values.yaml stable/redis
```

> **Tip**: 기본 [values.yaml](values.yaml)을 사용할 수 있습니다.
> **Note for minikube users**: minikube의 현재 버전 (작성 당시 v0.24.1)은 루트로만 쓸 수있는 `hostPath` 영구 볼륨을 제공합니다. 차트 기본값을 사용하면 Redis 포드가 `/bitnami` 디렉토리에 쓰려고 시도 할 때 포드 실패가 발생합니다. `--set persistence.enabled=false`로 Redis를 설치하는 것을 고려하십시오. 자세한 내용은 minikube issue [1990](https://github.com/kubernetes/minikube/issues/1990)를 참조하십시오.

## 네트워크 정책

Redis에 대한 네트워크 정책을 활성화하려면 [a networking plugin that implements the Kubernetes NetworkPolicy spec](https://kubernetes.io/docs/tasks/administer-cluster/declare-network-policy#before-you-begin)을 설치하고 `networkPolicy.enabled`를 `true`로 설정하십시오.

Kubernetes v1.5 및 v1.6의 경우 DefaultDeny 네임 스페이스 주석을 설정하여 NetworkPolicy도 설정해야합니다.
참고 : 네임 스페이스에서 _all_ 포드에 대한 정책이 적용됩니다.

    kubectl annotate namespace default "net.beta.kubernetes.io/network-policy={\"ingress\":{\"isolation\":\"DefaultDeny\"}}"

NetworkPolicy를 활성화하면 생성 된 클라이언트 레이블이있는 포드 만 Redis에 연결할 수 있습니다. 이 레이블은 성공적인 설치 후 출력에 표시됩니다.

## 영구적

[Bitnami Redis](https://github.com/bitnami/bitnami-docker-redis) 이미지는 Redis 데이터 및 구성을 컨테이너의 `/bitnami` 경로에 저장합니다.

기본적으로 차트는 이 위치에 [Persistent Volume](http://kubernetes.io/docs/user-guide/persistent-volumes/)을 마운트합니다. 볼륨은 동적 볼륨 프로비저닝을 사용하여 생성됩니다. 영구 볼륨 클레임이 이미 있으면 설치 중에 지정하십시오.

기본적으로 차트는 데이터와 구성을 모두 유지합니다. 데이터 디렉토리 만 유지하려면 `persistence.path`를 `/bitnami/redis/data`로, `persistence.subPath`를 `redis /data`로 설정하십시오.

### 기존 영구 볼륨 볼륨

1. PersistentVolume 생성
1. PersistentVolumeClaim 생성
1. 차트를 설치

```bash
$ helm install --set persistence.existingClaim=PVC_NAME redis
```

## 측정 항목

차트는 선택적으로 [prometheus](https://prometheus.io)에 대한 메트릭 내보내기를 시작할 수 있습니다. 메트릭 엔드포인트(포트 9121)가 서비스에 노출됩니다. [example Prometheus scrape configuration](https://github.com/prometheus/prometheus/blob/master/documentation/examples/prometheus-kubernetes.yml)에 설명 된 것과 유사한 것을 사용하여 클러스터 내에서 메트릭을 스크랩 할 수 있습니다. 클러스터 외부에서 메트릭을 스크랩해야하는 경우 Kubernetes API 프록시를 사용하여 엔드 포인트에 액세스 할 수 있습니다.