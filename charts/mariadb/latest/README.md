## 소개

이 차트는 [Helm](https://helm.sh) 패키지 관리자를 사용하여 [Kubernetes](http://kubernetes.io) 클러스터에서 [MariaDB](https://github.com/bitnami/bitnami-docker-mariadb) 복제 클러스터 배포를 부트 스트랩합니다.

## Configuration

다음 표는 MariaDB 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

|             Parameter                     |                     Description                     |                              Default                              |
|-------------------------------------------|-----------------------------------------------------|-------------------------------------------------------------------|
| `image.registry`                          | MariaDB image registry                              | `docker.io`                                                       |
| `image.repository`                        | MariaDB Image name                                  | `bitnami/mariadb`                                                 |
| `image.tag`                               | MariaDB Image tag                                   | `{VERSION}`                                                       |
| `image.pullPolicy`                        | MariaDB image pull policy                           | `Always` if `imageTag` is `latest`, else `IfNotPresent`           |
| `image.pullSecrets`                       | Specify image pull secrets                          | `nil` (does not add image pull secrets to deployed pods)          |
| `service.type`                            | Kubernetes service type                             | `ClusterIP`                                                       |
| `service.clusterIp`                       | Specific cluster IP when service type is cluster IP. Use None for headless service | `nil`                                                                  |
| `service.port`                            | MySQL service port                                  | `3306`                                                             |
| `rootUser.password`                       | Password for the `root` user                        | _random 10 character alphanumeric string_                         |
| `rootUser.forcePassword`                  | Force users to specify a password                   | `false`                                                           |
| `db.user`                                 | Username of new user to create                      | `nil`                                                             |
| `db.password`                             | Password for the new user                           | _random 10 character alphanumeric string if `db.user` is defined_ |
| `db.name`                                 | Name for new database to create                     | `my_database`                                                     |
| `replication.enabled`                     | MariaDB replication enabled                         | `true`                                                             |
| `replication.user`                        | MariaDB replication user                            | `replicator`                                                       |
| `replication.password`                    | MariaDB replication user password                   | _random 10 character alphanumeric string_                         |
| `master.annotations[].key`                | key for the the annotation list item                |  `nil`                                                  |
| `master.annotations[].value`              | value for the the annotation list item              |  `nil`                                                  |
| `master.affinity`                         | Master affinity (in addition to master.antiAffinity when set)  | `{}`                                                   |
| `master.antiAffinity`                     | Master pod anti-affinity policy                     | `soft`                                                            |
| `master.tolerations`                      | List of node taints to tolerate (master)            | `[]`                                                              |
| `master.persistence.enabled`              | Enable persistence using a `PersistentVolumeClaim`  | `true`                                                            |
| `master.persistence.existingClaim`        | Provide an existing `PersistentVolumeClaim`  | `nil`
| `master.persistence.mountPath`            | Configure existing `PersistentVolumeClaim` mount path  | `""`  
| `master.persistence.annotations`          | Persistent Volume Claim annotations                 | `{}`                                                              |
| `master.persistence.storageClass`         | Persistent Volume Storage Class                     | ``                                                                |
| `master.persistence.accessModes`          | Persistent Volume Access Modes                      | `[ReadWriteOnce]`                                                 |
| `master.persistence.size`                 | Persistent Volume Size                              | `8Gi`                                                             |
| `master.config`                           | Config file for the MariaDB Master server           | `_default values in the values.yaml file_`                        |
| `master.resources`                        | CPU/Memory resource requests/limits for master node | `{}`                                                              |
| `master.livenessProbe.enabled`            | Turn on and off liveness probe (master)             | `true`                                                            |
| `master.livenessProbe.initialDelaySeconds`| Delay before liveness probe is initiated (master)   | `120`                                                             |
| `master.livenessProbe.periodSeconds`      | How often to perform the probe (master)             | `10`                                                              |
| `master.livenessProbe.timeoutSeconds`     | When the probe times out (master)                   | `1`                                                               |
| `master.livenessProbe.successThreshold`   | Minimum consecutive successes for the probe (master)| `1`                                                               |
| `master.livenessProbe.failureThreshold`   | Minimum consecutive failures for the probe (master) | `3`                                                               |
| `master.readinessProbe.enabled`           | Turn on and off readiness probe (master)            | `true`                                                            |
| `master.readinessProbe.initialDelaySeconds`| Delay before readiness probe is initiated (master) | `30`                                                              |
| `master.readinessProbe.periodSeconds`     | How often to perform the probe (master)             | `10`                                                              |
| `master.readinessProbe.timeoutSeconds`    | When the probe times out (master)                   | `1`                                                               |
| `master.readinessProbe.successThreshold`  | Minimum consecutive successes for the probe (master)| `1`                                                               |
| `master.readinessProbe.failureThreshold`  | Minimum consecutive failures for the probe (master) | `3`                                                               |
| `slave.replicas`                          | Desired number of slave replicas                    | `1`                                                               |
| `slave.annotations[].key`                 | key for the the annotation list item                | `nil`                                                   |
| `slave.annotations[].value`               | value for the the annotation list item              | `nil`                                                   |
| `slave.affinity`                          | Slave affinity (in addition to slave.antiAffinity when set) | `{}`                                                      |
| `slave.antiAffinity`                      | Slave pod anti-affinity policy                      | `soft`                                                            |
| `slave.tolerations`                       | List of node taints to tolerate for (slave)         | `[]`                                                              |
| `slave.persistence.enabled`               | Enable persistence using a `PersistentVolumeClaim`  | `true`                                                            |
| `slave.persistence.annotations`           | Persistent Volume Claim annotations                 | `{}`                                                              |
| `slave.persistence.storageClass`          | Persistent Volume Storage Class                     | ``                                                                |
| `slave.persistence.accessModes`           | Persistent Volume Access Modes                      | `[ReadWriteOnce]`                                                 |
| `slave.persistence.size`                  | Persistent Volume Size                              | `8Gi`                                                             |
| `slave.config`                            | Config file for the MariaDB Slave replicas          | `_default values in the values.yaml file_`                        |
| `slave.resources`                         | CPU/Memory resource requests/limits for slave node  | `{}`                                                              |
| `slave.livenessProbe.enabled`             | Turn on and off liveness probe (slave)              | `true`                                                            |
| `slave.livenessProbe.initialDelaySeconds` | Delay before liveness probe is initiated (slave)    | `120`                                                             |
| `slave.livenessProbe.periodSeconds`       | How often to perform the probe (slave)              | `10`                                                              |
| `slave.livenessProbe.timeoutSeconds`      | When the probe times out (slave)                    | `1`                                                               |
| `slave.livenessProbe.successThreshold`    | Minimum consecutive successes for the probe (slave) | `1`                                                               |
| `slave.livenessProbe.failureThreshold`    | Minimum consecutive failures for the probe (slave)  | `3`                                                               |
| `slave.readinessProbe.enabled`            | Turn on and off readiness probe (slave)             | `true`                                                            |
| `slave.readinessProbe.initialDelaySeconds`| Delay before readiness probe is initiated (slave)   | `45`                                                              |
| `slave.readinessProbe.periodSeconds`      | How often to perform the probe (slave)              | `10`                                                              |
| `slave.readinessProbe.timeoutSeconds`     | When the probe times out (slave)                    | `1`                                                               |
| `slave.readinessProbe.successThreshold`   | Minimum consecutive successes for the probe (slave) | `1`                                                               |
| `slave.readinessProbe.failureThreshold`   | Minimum consecutive failures for the probe (slave)  | `3`                                                               |
| `metrics.enabled`                         | Start a side-car prometheus exporter                | `false`                                                           |
| `metrics.image.registry`                           | Exporter image registry                                 | `docker.io` |
`metrics.image.repository`                           | Exporter image name                                 | `prom/mysqld-exporter`                                            |
| `metrics.image.tag`                        | Exporter image tag                                  | `v0.10.0`                                                         |
| `metrics.image.pullPolicy`                 | Exporter image pull policy                          | `IfNotPresent`                                                    |
| `metrics.resources`                       | Exporter resource requests/limit                    | `nil`                                                             |

위의 매개 변수는 [bitnami/mariadb](http://github.com/bitnami/bitnami-docker-mariadb)에 정의 된 env 변수에 매핑됩니다. 자세한 내용은 [bitnami/mariadb](http://github.com/bitnami/bitnami-docker-mariadb) 이미지 설명서를 참조하십시오.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```bash
$ helm install --name my-release \
  --set root.password=secretpassword,user.database=app_database \
    stable/mariadb
```

위 명령은 MariaDB `root` 계정 암호를 `secretpassword`로 설정합니다. 또한 `my_database`이라는 데이터베이스를 만듭니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
$ helm install --name my-release -f values.yaml stable/mariadb
```

> **Tip**: 기본 [values.yaml](values.yaml)을 사용할 수 있습니다.

## 새로운 인스턴스를 초기화

[Bitnami MariaDB](https://github.com/bitnami/bitnami-docker-mariadb) 이미지를 사용하면 사용자 지정 스크립트를 사용하여 새로운 인스턴스를 초기화 할 수 있습니다. 스크립트를 실행하려면 차트 폴더 `files/docker-entrypoint-initdb.d` 내에 있어야 스크립트가 ConfigMap으로 사용될 수 있습니다.

허용되는 확장명은 `.sh`, `.sql` 및 `.sql.gz`입니다.

## 영구적

[Bitnami MariaDB](https://github.com/bitnami/bitnami-docker-mariadb) 이미지는 MariaDB 데이터와 구성을 컨테이너의 `/bitnami/mariadb` 경로에 저장합니다.

이 차트는이 위치에 [Persistent Volume](kubernetes.io/docs/user-guide/persistent-volumes/) 볼륨을 마운트합니다. 볼륨은 기본적으로 동적 볼륨 프로비저닝을 사용하여 생성됩니다. 기존 PersistentVolumeClaim을 정의 할 수 있습니다.

## 업그레이드

준비/생존 프로브가 올바르게 작동하도록 업그레이드 할 때`rootUser.password` 매개 변수를 설정해야합니다. 이 차트를 처음 설치하면 '관리자 자격 증명'섹션에서 사용해야하는 자격 증명을 제공하는 일부 참고 사항이 표시됩니다. 비밀번호를 적어두고 아래 명령을 실행하여 차트를 업그레이드하십시오:

```bash
$ helm upgrade my-release stable/mariadb --set rootUser.password=[ROOT_PASSWORD]
```

| Note: placeholder _ [ROOT_PASSWORD] _를 설치 정보에서 얻은 값으로 대체해야합니다.

### 5.0.0까지

차트 배포에 사용 된 레이블을 수정하지 않으면 이전 버전과의 호환성이 보장되지 않습니다.
아래 해결 방법을 사용하여 5.0.0 이전 버전에서 업그레이드하십시오. 다음 예제에서는 릴리스 이름이 mariadb라고 가정합니다.:

```console
$ kubectl delete statefulset opencart-mariadb --cascade=false
```
