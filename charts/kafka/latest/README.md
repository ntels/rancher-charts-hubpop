# 아파치 Kafka 헬름 차트

이것은 여기에서 찾은 Kafka StatefulSet의 구현법이다.

 * https://github.com/Yolean/kubernetes-kafka

## StatefulSet Caveats

* https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#limitations

### Installing the Chart
이 차트는 기본적으로 `requirement.yaml`의 kafka 클러스터에 대한 의존도로서 ZooKeeper 차트를 포함하고 있다. 다음 구성 가능한 매개 변수를 사용하여 차트를 사용자 정의할 수 있음:

| Parameter                      | Description                                                                                                     | Default                                                    |
| ------------------------------ | --------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------- |
| `image`                        | Kafka Container image name                                                                                      | `confluentinc/cp-kafka`                                    |
| `imageTag`                     | Kafka Container image tag                                                                                       | `4.0.0`                                                    |
| `imagePullPolicy`              | Kafka Container pull policy                                                                                     | `IfNotPresent`                                             |
| `replicas`                     | Kafka Brokers                                                                                                   | `3`                                                        |
| `component`                    | Kafka k8s selector key                                                                                          | `kafka`                                                    |
| `resources`                    | Kafka resource requests and limits                                                                              | `{}`                                                       |
| `kafkaHeapOptions`             | Kafka broker JVM heap options                                                                                   | `-Xmx1G-Xms1G`                                                       |
| `logSubPath`                   | Subpath under `persistence.mountPath` where kafka logs will be placed.                                          | `logs`                                                     |
| `schedulerName`                | Name of Kubernetes scheduler (other than the default)                                                           | `nil`                                                      |
| `affinity`                     | Defines affinities and anti-affinities for pods as defined in: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity preferences                                                                                      | `{}`                                                       |
| `tolerations`                  | List of node tolerations for the pods. https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/  | `[]`                                                       |
| `external.enabled`             | If True, exposes Kafka brokers via NodePort (PLAINTEXT by default)                                              | `false`                                                    |
| `external.servicePort`         | TCP port configured at external services (one per pod) to relay from NodePort to the external listener port.    | '19092'                                                    |
| `external.firstListenerPort`   | TCP port which is added pod index number to arrive at the port used for NodePort and external listener port.    | '31090'                                                    |
| `external.domain`              | Domain in which to advertise Kafka external listeners.                                                          | `cluster.local`                                            |
| `external.init`                | External init container settings.                                                                               | (see `values.yaml`)                                        |
| `rbac.enabled`                 | Enable a service account and role for the init container to use in an RBAC enabled cluster                      | `false`                                                    |
| `configurationOverrides`       | `Kafka ` [configuration setting][brokerconfigs] overrides in the dictionary format                              | `{ offsets.topic.replication.factor: 3 }`                  |
| `additionalPorts`              | Additional ports to expose on brokers.  Useful when the image exposes metrics (like prometheus, etc.) through a javaagent instead of a sidecar   | `{}`                                 |
| `readinessProbe.initialDelaySeconds` | Number of seconds before probe is initiated.                                                              | `30`                                                       |
| `readinessProbe.periodSeconds`       | How often (in seconds) to perform the probe.                                                              | `10`                                                       |
| `readinessProbe.timeoutSeconds`      | Number of seconds after which the probe times out.                                                        | `5`                                                        |
| `readinessProbe.successThreshold`    | Minimum consecutive successes for the probe to be considered successful after having failed.              | `1`                                                        |
| `readinessProbe.failureThreshold`    | After the probe fails this many times, pod will be marked Unready.                                        | `3`                                                        |
| `terminationGracePeriodSeconds`      | Wait up to this many seconds for a broker to shut down gracefully, after which it is killed               | `60`                                                        |
| `updateStrategy`               | StatefulSet update strategy to use.                                                                             | `{ type: "OnDelete" }`                                     |
| `podManagementPolicy`          | Start and stop pods in Parallel or OrderedReady (one-by-one.)  Can not change after first release.              | `OrderedReady`                                     |
| `persistence.enabled`          | Use a PVC to persist data                                                                                       | `true`                                                     |
| `persistence.size`             | Size of data volume                                                                                             | `1Gi`                                                      |
| `persistence.mountPath`        | Mount path of data volume                                                                                       | `/opt/kafka/data`                                          |
| `persistence.storageClass`     | Storage class of backing PVC                                                                                    | `nil`                                                      |
| `jmx.configMap.enabled`      | Enable the default ConfigMap for JMX                                                                   | `true`                                                     |
| `jmx.configMap.overrideConfig` | Allows config file to be generated by passing values to ConfigMap                                     | `{}`                                                       |
| `jmx.configMap.overrideName` | Allows setting the name of the ConfigMap to be used                                                     | `""`                                                       |
| `jmx.port`                      | The jmx port which JMX style metrics are exposed (note: these are not scrapeable by Prometheus)                 | `5555`                                                     |
| `jmx.whitelistObjectNames` | Allows setting which JMX objects you want to expose to via JMX stats to JMX Exporter                      | (see `values.yaml`)                                                       |
| `prometheus.jmx.resources`      | Allows setting resource limits for jmx sidecar container                                                        | `{}`                                                       |
| `prometheus.jmx.enabled`          | Whether or not to expose JMX metrics to Prometheus                                                           | `false`                                                    |
| `prometheus.jmx.image`            | JMX Exporter container image                                                                                 | `solsson/kafka-prometheus-jmx-exporter@sha256`             |
| `prometheus.jmx.imageTag`         | JMX Exporter container image tag                                                                             | `a23062396cd5af1acdf76512632c20ea6be76885dfc20cd9ff40fb23846557e8` |
| `prometheus.jmx.interval`         | Interval that Prometheus scrapes JMX metrics when using Prometheus Operator                                  | `10s`                                                     |
| `prometheus.jmx.port`             | JMX Exporter Port which exposes metrics in Prometheus format for scraping                                    | `5556`                                                     |
| `prometheus.kafka.enabled`        | Whether or not to create a separate Kafka exporter                                                           | `false`                                                    |
| `prometheus.kafka.image`          | Kafka Exporter container image                                                                               | `danielqsj/kafka-exporter`                                 |
| `prometheus.kafka.imageTag`       | Kafka Exporter container image tag                                                                           | `v1.0.1`                                                   |
| `prometheus.kafka.interval`       | Interval that Prometheus scrapes Kafka metrics when using Prometheus Operator                                | `10s`                                                      |
| `prometheus.kafka.port`        | Kafka Exporter Port which exposes metrics in Prometheus format for scraping                                     | `9308`                                                     |
| `prometheus.kafka.resources`    | Allows setting resource limits for kafka-exporter pod                                                           | `{}`                                                       |
| `prometheus.operator` | True if using the Prometheus Operator, False if not                                                             | `false`                                                       |
| `prometheus.operator.serviceMonitor.namespace` | Namespace which Prometheus is running in.  Default to kube-prometheus install.    | `monitoring` |
| `prometheus.operator.serviceMonitor.selector` | Default to kube-prometheus install (CoreOS recommended), but should be set according to Prometheus install    | `{ prometheus: kube-prometheus }` |
| `zookeeper.enabled`            | If True, installs Zookeeper Chart                                                                               | `true`                                                     |
| `zookeeper.resources`          | Zookeeper resource requests and limits                                                                          | `{}`                                                       |
| `zookeeper.heap`               | JVM heap size to allocate to Zookeeper                                                                          | `1G`                                                       |
| `zookeeper.storage`            | Zookeeper Persistent volume size                                                                                | `2Gi`                                                      |
| `zookeeper.imagePullPolicy`    | Zookeeper Container pull policy                                                                                 | `IfNotPresent`                                             |
| `zookeeper.url`                | URL of Zookeeper Cluster (unneeded if installing Zookeeper Chart)                                               | `""`                                                       |
| `zookeeper.port`               | Port of Zookeeper Cluster                                                                                       | `2181`                                                     |
| `zookeeper.affinity`                     | Defines affinities and anti-affinities for pods as defined in: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity preferences                                                                                      | `{}`

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면:

```bash
$ helm install --name my-kafka -f values.yaml incubator/kafka
```

### Kubernetes 내부에서 Kafka로 연결

다음과 같은 구성으로 K8s 클러스터에서 다음과 같은 간단한 포드를 실행하면 카프카에 연결할 수 있다:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: testclient
  namespace: kafka
spec:
  containers:
  - name: kafka
    image: solsson/kafka:0.11.0.0
    command:
      - sh
      - -c
      - "exec tail -f /dev/null"
```

위의 테스트 클라이언트 포드를 실행하면 다음과 같은 모든 카프카 항목을 나열할 수 있다:

` kubectl -n kafka exec -ti testclient -- ./bin/kafka-topics.sh --zookeeper
my-release-zookeeper:2181 --list`

여기서 `my-release`는 당신의 헬름 릴리즈 네임이다.

## 확장

카프카는 풍부한 생태계를 가지고 있고, 많은 도구들을 가지고 있다. 본 섹션은 해당 헬름 차트가 이미 작성된 모든 도구를 컴파일하기 위한 것이다.

- [Schema-registry](https://github.com/kubernetes/charts/tree/master/incubator/schema-registry) -  메타데이터에 대한 서비스 계층을 제공하는 결합 프로젝트. 그것은 Avro 스키마를 저장하고 검색하기 위한 RESTful 인터페이스를 제공한다.

### Kubernetes 외부에서 Kafka에 연결

`values.yaml`의 외부 액세스와 관련된 예제 텍스트를 사용하려면 검토하고 선택적으로 대체하십시오.

구성되면 복제 본당 하나씩 NodePorts를 통해 Kafka에 연결할 수 있어야합니다. 개인, 토폴로지가 사용되는 kops에서 이 기능은 다음 이름 지정 체계를 사용하여 내부 라운드 로빈 DNS 레코드를 게시합니다. 이 차트의 외부 접근 기능은 플란넬 네트워킹을 사용하여 AWS의 kops로 테스트되었다.
kops로 실행되는 카프카에 대한 외부 접근을 가능하게 하려면, 당신의 보안 그룹을 비 쿠버넷 노드(예: bastion)가 카프카 외부 수신기 포트 범위에 접근할 수 있도록 조정해야 할 것이다.

```
{{ .Release.Name }}.{{ .Values.external.domain }}
```

컨테이너와 NodePort에서 사용되는 외부 접근을 위한 포트 번호는 StatefulSet의 각 컨테이너에 고유하다.
`replicas` 값이 `3`인 기본 `external.firstListenerPort` 번호를 사용하여 외부 액세스를 위해 다음 컨테이너와 NodePorts를 여십시오: `31090`, `31091`, `31092`. 쿠버네티스는 각 노드포트를 동일한 포트에서 수신하는 엔트리 노드에서 포드/컨테이너로 라우팅하기 때문에 이러한 모든 포트는 호스트에서 노드포트로 연결할 수 있어야 한다(예: '31091').

각 외부 접속 서비스(팟당 하나의 서비스)의 `external.servicePort`는 각각의 `NodePort`와 일치하는 번호를 가진 `external.servicePort` 쪽으로 중계되는 것이다. NodePort의 범위는 StatefulSet의 모든 Kafka 포드에서 설정되지만 실제로 수신해서는 안됩니다. 특정 포드는 한 번에 하나의 포트만 수신하므로 모든 Kafka 포드에서 범위를 설정하는 것이 합리적으로 안전한 구성입니다.

## 알려진 제한

* 주제 작성이 자동화되지 않습니다.
* 영구 볼륨 클레임에 대한 백엔드가있는 스토리지 옵션 만 지원 (대부분 AWS에서 테스트)
* 동일한 네임 스페이스에 `kafka`라는 서비스가 없어야합니다.

[brokerconfigs]: https://kafka.apache.org/documentation/#brokerconfigs

## Prometheus 통계

### Prometheus vs Prometheus Operator

Standard Prometheus가 이 차트의 기본 모니터링 옵션입니다. 이 차트는 CoreOS Prometheus Operator를 지원하며 Prometheus 및 Alert Manager 구성 자동 업데이트와 같은 추가 기능을 제공 할 수 있습니다. Prometheus Operator 설치에 관심이있는 경우 자세한 내용은 [CoreOS repository](https://github.com/coreos/prometheus-operator/tree/master/helm)를 참조하거나 [CoreOS blog post introducing the Prometheus Operator](https://coreos.com/blog/the-prometheus-operator.html)를 읽으십시오.

### JMX Exporter

Kafka 통계의 대부분은 JMX를 통해 제공되며 [Prometheus JMX Exporter](https://github.com/prometheus/jmx_exporter)를 통해 노출됩니다.

JMX Exporter는 모든 Java 응용 프로그램과 함께 사용하기 위한 범용 프로 메테우스 공급자입니다. 이로 인해 관심이 없을 수 있는 많은 통계가 생성됩니다. 이러한 통계를 관련 구성 요소로 줄이는 데 도움을 주기 위해 JMX 내보내기를 위한 선별 된 화이트리스트 `whitelistObjectNames`을 작성했습니다. 이 화이트리스트는 값 구성을 통해 수정하거나 제거 할 수 있습니다.

이 차트는 Prometheus 지표와의 호환성을 위해 원시 JMX 지표의 변환을 수행합니다. 예를 들어, 브로커 이름 및 주제 이름이 라벨이 아닌 측정 항목 이름으로 통합되었습니다.
차트 메트릭에 대한 기본 변환에 대해 더 궁금한 점이 있으면 [configmap template](https://github.com/kubernetes/charts/blob/master/incubator/kafka/templates/jmx-configmap.yaml)를 참조하십시오.

### Kafka Exporter

[Kafka Exporter](https://github.com/danielqsj/kafka_exporter)은 JMX Exporter에 대한 무료 메트릭스 익스포터입니다. Kafka Exporter는 Kafka Consumer Groups에 대한 추가 통계를 제공합니다.
