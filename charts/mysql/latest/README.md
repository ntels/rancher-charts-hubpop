## 전제 조건

- 베타 API가 활성화 된 Kubernetes 1.6 이상
- 기본 인프라에서 PV 프로비저너 지원

## Chart 설치

기본적으로 루트 사용자에 대해 임의의 비밀번호가 생성됩니다. 자신의 암호를 설정하려면 values.yaml에서 mysqlRootPassword를 변경하십시오.

다음 명령을 실행하여 루트 비밀번호를 검색 할 수 있습니다. [YOUR_RELEASE_NAME]을(를) 교체해야합니다:

    printf $(printf '\%o' `kubectl get secret [YOUR_RELEASE_NAME]-mysql -o jsonpath="{.data.mysql-root-password[*]}"`)

> **Tip**: `helm list`를 사용하여 모든 릴리즈를 나열하십시오.

## 구성

다음 표는 MySQL 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

| Parameter                            | Description                               | Default                                              |
| ------------------------------------ | ----------------------------------------- | ---------------------------------------------------- |
| `imageTag`                           | `mysql` image tag.                        | Most recent release                                  |
| `imagePullPolicy`                    | Image pull policy                         | `IfNotPresent`                                       |
| `mysqlRootPassword`                  | Password for the `root` user.             | `nil`                                                |
| `mysqlUser`                          | Username of new user to create.           | `nil`                                                |
| `mysqlPassword`                      | Password for the new user.                | `nil`                                                |
| `mysqlDatabase`                      | Name for new database to create.          | `nil`                                                |
| `livenessProbe.initialDelaySeconds`  | Delay before liveness probe is initiated  | 30                                                   |
| `livenessProbe.periodSeconds`        | How often to perform the probe            | 10                                                   |
| `livenessProbe.timeoutSeconds`       | When the probe times out                  | 5                                                    |
| `livenessProbe.successThreshold`     | Minimum consecutive successes for the probe to be considered successful after having failed. | 1 |
| `livenessProbe.failureThreshold`     | Minimum consecutive failures for the probe to be considered failed after having succeeded.   | 3 |
| `readinessProbe.initialDelaySeconds` | Delay before readiness probe is initiated | 5                                                    |
| `readinessProbe.periodSeconds`       | How often to perform the probe            | 10                                                   |
| `readinessProbe.timeoutSeconds`      | When the probe times out                  | 1                                                    |
| `readinessProbe.successThreshold`    | Minimum consecutive successes for the probe to be considered successful after having failed. | 1 |
| `readinessProbe.failureThreshold`    | Minimum consecutive failures for the probe to be considered failed after having succeeded.   | 3 |
| `persistence.enabled`                | Create a volume to store data             | true                                                 |
| `persistence.size`                   | Size of persistent volume claim           | 8Gi RW                                               |
| `nodeSelector`                       | Node labels for pod assignment            | {}                                                   |
| `persistence.storageClass`           | Type of persistent volume claim           | nil  (uses alpha storage class annotation)           |
| `persistence.accessMode`             | ReadWriteOnce or ReadOnly                 | ReadWriteOnce                                        |
| `persistence.existingClaim`          | Name of existing persistent volume        | `nil`                                                |
| `persistence.subPath`                | Subdirectory of the volume to mount       | `nil`                                                |
| `resources`                          | CPU/Memory resource requests/limits       | Memory: `256Mi`, CPU: `100m`                         |
| `configurationFiles`                 | List of mysql configuration files         | `nil`                                                |

위의 매개 변수 중 일부는 [MySQL DockerHub image](https://hub.docker.com/_/mysql/)에 정의 된 env 변수에 매핑됩니다.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```bash
$ helm install --name my-release \
  --set mysqlRootPassword=secretpassword,mysqlUser=my-user,mysqlPassword=my-password,mysqlDatabase=my-database \
    stable/mysql
```

위 명령은 MySQL `root` 계정 암호를 `secretpassword`로 설정합니다. 또한 `my-database`이라는 데이터베이스에 액세스 할 수있는 비밀번호가 `my-password` 인 `my-user`이라는 표준 데이터베이스 사용자를 작성합니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
$ helm install --name my-release -f values.yaml stable/mysql
```

> **Tip**: 기본 [values.yaml] (values.yaml)을 사용할 수 있습니다.

## 영구적

The [MySQL](https://hub.docker.com/_/mysql/) 이미지는 컨테이너의`/var/lib/mysql` 경로에 MySQL 데이터 및 구성을 저장합니다.

기본적으로 PersistentVolumeClaim이 생성되어 해당 디렉토리에 마운트됩니다. 이 기능을 비활성화하려면 지속성을 비활성화하고 emptyDir을 대신 사용하기 위해 values.yaml을 변경할 수 있습니다.

> *"emptyDir 볼륨은 포드가 노드에 할당 될 때 처음 생성되며 해당 노드에서 포드가 실행되는 동안 존재합니다. 어떤 이유로 든 포드가 노드에서 제거되면 emptyDir의 데이터가 영구적으로 삭제됩니다."*

## 사용자 정의 MySQL 설정 파일

[MySQL](https://hub.docker.com/_/mysql/) 이미지는`/etc/mysql/conf.d` 경로에서 사용자 정의 구성 파일을 허용합니다. 사용자 정의 된 MySQL 구성을 사용하려면 `configurationFiles` 속성에 파일 내용을 전달하여 대체 구성 파일을 작성할 수 있습니다. MySQL 문서에 따르면`.cnf`로 끝나는 파일 만 로드됩니다.

```yaml
configurationFiles:
  mysql.cnf: |-
    [mysqld]
    skip-host-cache
    skip-name-resolve
    sql-mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
  mysql_custom.cnf: |-
    [mysqld]
```
