## 전제 조건

- 베타 API가 활성화 된 Kubernetes 1.4 이상
- 기본 인프라에서 PV 프로비저너 지원

## 구성

다음 표는 MongoDB 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

| Parameter                               | Description                                                                                  | Default                                     |
| --------------------------------------- | -------------------------------------------------------------------------------------------- | ------------------------------------------- |
| `global.imageRegistry`                  | Global Docker image registry                                                                 | `nil`                                       |
| `image.registry`                        | MongoDB image registry                                                                       | `docker.io`                                 |
| `image.repository`                      | MongoDB Image name                                                                           | `bitnami/mongodb`                           |
| `image.tag`                             | MongoDB Image tag                                                                            | `{VERSION}`                                 |
| `image.pullPolicy`                      | Image pull policy                                                                            | `Always`                                    |
| `image.pullSecrets`                     | Specify image pull secrets                                                                   | `nil`                                       |
| `usePassword`                           | Enable password authentication                                                               | `true`                                      |
| `existingSecret`                        | Existing secret with MongoDB credentials                                                     | `nil`                                       |
| `mongodbRootPassword`                   | MongoDB admin password                                                                       | `random alhpanumeric string (10)`           |
| `mongodbUsername`                       | MongoDB custom user                                                                          | `nil`                                       |
| `mongodbPassword`                       | MongoDB custom user password                                                                 | `random alhpanumeric string (10)`           |
| `mongodbDatabase`                       | Database to create                                                                           | `nil`                                       |
| `mongodbEnableIPv6`                     | Switch to enable/disable IPv6 on MongoDB                                                     | `true`                                      |
| `mongodbExtraFlags`                     | MongoDB additional command line flags                                                        | []                                          |
| `service.annotations`                   | Kubernetes service annotations                                                               | `{}`                                        |
| `service.type`                          | Kubernetes Service type                                                                      | `ClusterIP`                                 |
| `service.clusterIP`                     | Static clusterIP or None for headless services                                               | `nil`                                       |
| `service.nodePort`                      | Port to bind to for NodePort service type                                                    | `nil`                                       |
| `port`                                  | MongoDB service port                                                                         | `27017`                                     |
| `replicaSet.enabled`                    | Switch to enable/disable replica set configuration                                           | `false`                                     |
| `replicaSet.name`                       | Name of the replica set                                                                      | `rs0`                                       |
| `replicaSet.useHostnames`               | Enable DNS hostnames in the replica set config                                               | `true`                                      |
| `replicaSet.key`                        | Key used for authentication in the replica set                                               | `nil`                                       |
| `replicaSet.replicas.secondary`         | Number of secondary nodes in the replica set                                                 | `1`                                         |
| `replicaSet.replicas.arbiter`           | Number of arbiter nodes in the replica set                                                   | `1`                                         |
| `replicaSet.pdb.minAvailable.primary`   | PDB for the MongoDB Primary nodes                                                            | `1`                                         |
| `replicaSet.pdb.minAvailable.secondary` | PDB for the MongoDB Secondary nodes                                                          | `1`                                         |
| `replicaSet.pdb.minAvailable.arbiter`   | PDB for the MongoDB Arbiter nodes                                                            | `1`                                         |
| `podAnnotations`                        | Annotations to be added to pods                                                              | {}                                          |
| `podLabels`                             | Additional labels for the pod(s).                                                            | {}                                          |
| `resources`                             | Pod resources                                                                                | {}                                          |
| `nodeSelector`                          | Node labels for pod assignment                                                               | {}                                          |
| `affinity`                              | Affinity for pod assignment                                                                  | {}                                          |
| `tolerations`                           | Toleration labels for pod assignment                                                         | {}                                          |
| `securityContext.enabled`               | Enable security context                                                                      | `true`                                      |
| `securityContext.fsGroup`               | Group ID for the container                                                                   | `1001`                                      |
| `securityContext.runAsUser`             | User ID for the container                                                                    | `1001`                                      |
| `persistence.enabled`                   | Use a PVC to persist data                                                                    | `true`                                      |
| `persistence.storageClass`              | Storage class of backing PVC                                                                 | `nil` (uses alpha storage class annotation) |
| `persistence.accessMode`                | Use volume as ReadOnly or ReadWrite                                                          | `ReadWriteOnce`                             |
| `persistence.size`                      | Size of data volume                                                                          | `8Gi`                                       |
| `persistence.annotations`               | Persistent Volume annotations                                                                | `{}`                                        |
| `persistence.existingClaim`             | Name of an existing PVC to use (avoids creating one if this is given)                        | `nil`                                       |
| `livenessProbe.initialDelaySeconds`     | Delay before liveness probe is initiated                                                     | `30`                                        |
| `livenessProbe.periodSeconds`           | How often to perform the probe                                                               | `10`                                        |
| `livenessProbe.timeoutSeconds`          | When the probe times out                                                                     | `5`                                         |
| `livenessProbe.successThreshold`        | Minimum consecutive successes for the probe to be considered successful after having failed. | `1`                                         |
| `livenessProbe.failureThreshold`        | Minimum consecutive failures for the probe to be considered failed after having succeeded.   | `6`                                         |
| `readinessProbe.initialDelaySeconds`    | Delay before readiness probe is initiated                                                    | `5`                                         |
| `readinessProbe.periodSeconds`          | How often to perform the probe                                                               | `10`                                        |
| `readinessProbe.timeoutSeconds`         | When the probe times out                                                                     | `5`                                         |
| `readinessProbe.failureThreshold`       | Minimum consecutive failures for the probe to be considered failed after having succeeded.   | `6`                                         |
| `readinessProbe.successThreshold`       | Minimum consecutive successes for the probe to be considered successful after having failed. | `1`                                         |
| `configmap`                             | MongoDB configuration file to be used                                                        | `nil`                                       |
| `metrics.enabled`                       | Start a side-car prometheus exporter                                                         | `false`                                     |
| `metrics.image.registry`                | MongoDB exporter image registry                                                              | `docker.io`                                 |
| `metrics.image.repository`              | MongoDB exporter image name                                                                  | `forekshub/percona-mongodb-exporter`                           |
| `metrics.image.tag`                     | MongoDB exporter image tag                                                                   | `latest`                                    |
| `metrics.image.pullPolicy`              | Image pull policy                                                                            | `IfNotPresent`                              |
| `metrics.image.pullSecrets`             | Specify docker-registry secret names as an array                                             | `nil`                                       |
| `metrics.podAnnotations`                | Additional annotations for Metrics exporter pod                                              | {}                                          |
| `metrics.resources`                     | Exporter resource requests/limit                                                             | Memory: `256Mi`, CPU: `100m`             |
| `metrics.serviceMonitor.enabled`        | Create ServiceMonitor Resource for scraping metrics using PrometheusOperator                 | `false`                                     |
| `metrics.serviceMonitor.additionalLabels`          | Used to pass Labels that are required by the Installed Prometheus Operator        | {}                                          |
| `metrics.serviceMonitor.relabellings`              | Specify Metric Relabellings to add to the scrape endpoint                         | `nil`                                       |
| `metrics.serviceMonitor.alerting.rules`            | Define individual alerting rules as required                                      | {}                                          |
| `metrics.serviceMonitor.alerting.additionalLabels` | Used to pass Labels that are required by the Installed Prometheus Operator        | {}                                          |


`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```bash
$ helm install --name my-release \
  --set mongodbRootPassword=secretpassword,mongodbUsername=my-user,mongodbPassword=my-password,mongodbDatabase=my-database \
    stable/mongodb
```

위 명령은 MongoDB `root` 계정 암호를 `secretpassword`로 설정합니다. 또한 `my-database`이라는 데이터베이스에 액세스 할 수있는 비밀번호가`my-password` 인 `my-user`이라는 표준 데이터베이스 사용자를 작성합니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
$ helm install --name my-release -f values.yaml stable/mongodb
```

> **Tip**: 기본값 [values.yaml](values.yaml)을 사용할 수 있습니다.

## 복제

다음 명령을 사용하여 레플리카 세트 모드에서 MongoDB 차트를 시작할 수 있습니다:

```bash
$ helm install --name my-release stable/mongodb --set replication.enabled=true
```

## 생산 설정 및 수평 스케일링

[values-production.yaml](values-production.yaml) 파일은 프로덕션 환경에 확장 가능하고 고 가용성 MongoDB 배치를 배치하기위한 구성으로 구성됩니다. 이 템플릿을 기반으로 프로덕션 구성을 설정하고 매개 변수를 적절하게 조정하는 것이 좋습니다.

```console
$ curl -O https://raw.githubusercontent.com/kubernetes/charts/master/stable/mongodb/values-production.yaml
$ helm install --name my-release -f ./values-production.yaml stable/mongodb
```

이 차트를 수평으로 확장하려면 다음 명령을 실행하여 MongoDB 레플리카 세트의 보조 노드 수를 조정하십시오.

```console
$ kubectl scale statefulset my-release-mongodb-secondary --replicas=3
```

이 차트의 일부 특징은 다음과 같습니다:

- 복제의 각 참가자는 stateful 세트를 가지고 있으므로 primary, secondary 또는 arbiter 노드를 찾을 위치를 항상 알 수 있습니다.
- secondary 및 arbiter 노드의 수는 독립적으로 확장 할 수 있습니다.
- Standalone MongoDB 서버를 사용하여 레플리카 세트를 사용하여 애플리케이션을 쉽게 이동할 수 있습니다.

## 새로운 인스턴스를 초기화

[Bitnami MongoDB](https://github.com/bitnami/bitnami-docker-mongodb) 이미지를 사용하면 사용자 지정 스크립트를 사용하여 새 인스턴스를 초기화 할 수 있습니다. 스크립트를 실행하려면 차트 폴더 `files/docker-entrypoint-initdb.d` 안에 있어야 스크립트가 ConfigMap으로 사용될 수 있습니다.

허용되는 확장자는`.sh`와`.js`입니다.

## 영구적

[Bitnami MongoDB](https://github.com/bitnami/bitnami-docker-mongodb) 이미지는 MongoDB 데이터 및 구성을 컨테이너의 `/bitnami/mongodb` 경로에 저장합니다.

이 차트는 이 위치에 [Persistent Volume](http://kubernetes.io/docs/user-guide/persistent-volumes/)을 마운트합니다. 볼륨은 동적 볼륨 프로비저닝을 사용하여 생성됩니다.

## 업그레이드

### 5.0.0까지

레플리카 세트 구성을 활성화 할 때 차트의 stateful 세트에 사용 된 레이블을 수정하지 않으면 이전 버전과의 호환성이 보장되지 않습니다.
아래 해결 방법을 사용하여 5.0.0 이전 버전에서 업그레이드하십시오. 다음 예제는 릴리스 이름이 `my-release`이라고 가정합니다:

```consoloe
$ kubectl delete statefulset my-release-mongodb-arbiter my-release-mongodb-primary my-release-mongodb-secondary --cascade=false
```
