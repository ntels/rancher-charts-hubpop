# JFrog Artifactory HA 헬름 차트

## 필수 구성 요소 세부 정보

* Kubernetes 1.8+
* Artifactory HA license

## 차트 상세 정보
이 차트는 다음을 수행할 것이다:

* Artifactory HA 클러스터 배포. 기본 노드 1개 및 멤버 노드 2개.
* PostgreSQL 데이터베이스 배포
* Nginx 서버 배포

## Artifactory HA 구조
이 차트의 Artifactory HA 클러스터는
- 단일 기본 노드
- 두 개의 멤버 노드(마음대로 크기 조정 가능)

로드 밸런싱은 멤버 노드에만 수행된다.
이로 인해 기본 노드는 작업과 태스크를 자유롭게 처리할 수 있으며 인바운드 트래픽에 의해 중단되지 않는다.
> 이것은 `artifactory.service.pool` 파라미터로 제어할 수 있다.

## 차트 설치

### JFrog 헬름 저장소 추가
JFrog 헬름 차트를 설치하기 전에, [JFrog 헬름 저장소](https://charts.jfrog.io/)를 헬름 클라이언트에 추가하십시오.
```bash
helm repo add jfrog https://charts.jfrog.io
```

### 설치 차트
`artifactory-ha` 릴리즈 이름을 사용하여 차트를 설치하려면 다음과 같이 하십시오. :
```bash
helm install --name artifactory-ha jfrog/artifactory-ha
```

### replicator를 사용하여 Artifactory 배포
[Artifactory replicator](https://www.jfrog.com/confluence/display/RTF/Replicator)는 [Enterprise Plus](https://www.jfrog.com/confluence/display/EP/Welcome+to+JFrog+Enterprise+Plus) 라이센스가 사용된다.
## Artifactory replicator는 기본적으로 비활성화되어 있다. replicator가 활성화될 때, replicator.publicUrl 파라미터가 필요하다. 활성화하려면 다음을 사용하십시오:
```bash
helm install --name artifactory --set artifactory.replicator.enabled=true --set artifactory.replicator.publicUrl=<artifactory_url>:<replicator_port> jfrog/artifactory-ha
```

### Artifactory 액세스
**NOTE:** Artifactory의 public IP를 사용할 수 있게 되고, 노드가 초기 설정을 완료하는데 몇 분 정도 걸릴 수 있다.
install 명령에 의해 출력된 지침에 따라 Artifactory IP 및 URL을 가져와 액세스 하십시오.

### Artifactory 업데이트
새로운 차트 버전을 갖게 되면, 다음 항목으로 배포를 업데이트할 수 있다.
```bash
helm upgrade artifactory-ha jfrog/artifactory-ha
```

만약 postgresql.postgresPassword 값을 제공하지 않고 Artifactory를 설치한 경우(암호가 자동 생성), 이 지시대로 하시오:
1. 실행하여 현재 암호 가져오기:
```bash
POSTGRES_PASSWORD=$(kubectl get secret -n <namespace> <myrelease>-postgresql -o jsonpath="{.data.postgres-password}" | base64 --decode)
```
2. 이전에 자동 생성된 암호를 전달하여 릴리즈 업그레이드:
```bash
helm upgrade <myrelease> jfrog/artifactory-ha --set postgresql.postgresPassword=${POSTGRES_PASSWORD}
```

기존 배포에 모든 구성 변경 사항이 적용된다.

### Artifactory 메모리와 CPU 리소스
Artifactory HA 헬름 차트에는 구성된 리소스 요청 및 모든 포드의 제한에 대한 지원이 제공된다. 기본적으로 이러한 설정은 설명되어 있다.
할당된 리소스와 제한을 완전히 제어할 수 있도록 이러한 리소스를 설정하는 것이 **매우** 권장된다.

자세한 내용은 [setting resources for your Artifactory based on planned usage](https://www.jfrog.com/confluence/display/RTF/System+Requirements#SystemRequirements-RecommendedHardware).

```bash
# 모든 포드에 대한 리소스 요청 및 제한 설정 예제(Artifactory에 Java 메모리 설정 전달 포함)
helm install --name artifactory-ha \
               --set artifactory.primary.resources.requests.cpu="500m" \
               --set artifactory.primary.resources.limits.cpu="2" \
               --set artifactory.primary.resources.requests.memory="1Gi" \
               --set artifactory.primary.resources.limits.memory="4Gi" \
               --set artifactory.primary.javaOpts.xms="1g" \
               --set artifactory.primary.javaOpts.xmx="4g" \
               --set artifactory.node.resources.requests.cpu="500m" \
               --set artifactory.node.resources.limits.cpu="2" \
               --set artifactory.node.resources.requests.memory="1Gi" \
               --set artifactory.node.resources.limits.memory="4Gi" \
               --set artifactory.node.javaOpts.xms="1g" \
               --set artifactory.node.javaOpts.xmx="4g" \
               --set postgresql.resources.requests.cpu="200m" \
               --set postgresql.resources.limits.cpu="1" \
               --set postgresql.resources.requests.memory="500Mi" \
               --set postgresql.resources.limits.memory="1Gi" \
               --set nginx.resources.requests.cpu="100m" \
               --set nginx.resources.limits.cpu="250m" \
               --set nginx.resources.requests.memory="250Mi" \
               --set nginx.resources.limits.memory="500Mi" \
               jfrog/artifactory-ha
```
> Artifactory 자바 메모리 파라미터는 할당된 리소스와 `artifactory.[primary|node].javaOpts.xms` 그리고 `artifactory.[primary|node].javaOpts.xmx`가 일치하도록 설정할 수 있고, 해야만 한다.

[official documentation](https://www.jfrog.com/confluence/)에서 Artifactory 구성에 대한 자세한 정보 보기.

### Artifactory 스토리지
Artifactory HA는 광범위한 스토리지 백엔드를 지원한다. [Artifactory HA storage options](https://www.jfrog.com/confluence/display/RTF/HA+Installation+and+Setup#HAInstallationandSetup-SettingUpYourStorageConfiguration)에서 자세한 정보를 볼 수 있다.

이 차트에서, 원하는 유형의 스토리지를 `artifactory.persistence.type`으로 설정하고, 필요한 구성 설정을 전달하십시오.
이 차트의 기본 스토리지는 데이터가 모든 노드에 복제되는 `file-system` 복제입니다.

> **IMPORTANT:** 모든 스토리지 구성(NFS 제외)에는 기본 `artifactory.persistence.redundancy` 파라미터가 포함되어 있다.
이것은 한 바이너리의 복제본 수를 클러스터의 노드에 저장할 수 있도록 설정하는 데 사용된다.
이 값이 배포 초기에 설정되면 헬름 차트를 사용하여 업데이트할 수 없다.
이 값을 클러스터 크기의 절반보다 큰 숫자로 설정하고 클러스터를 이 숫자보다 작은 크기로 축소하지 마십시오.

#### 기존 볼륨 클레임

###### 기본 노드
Artifactory Primary 스토리지에 대한 기존 볼륨 클레임을 사용하려면 다음 작업을 수행하십시오:
- 이름으로 영구 볼륨 클레임 생성 `volume-<release-name>-artifactory-ha-primary-0` 예시 `volume-myrelease-artifactory-ha-primary-0`
- `helm install`와 `helm upgrade`에 파라미터 전달
```bash
...
--set artifactory.primary.persistence.existingClaim=true
```

###### 멤버 노드
Artifactory 멤버 노드 스토리지에 대한 기존 볼륨 클레임을 사용하려면:
- 이름 `volume-<release-name>-artifactory-ha-member-<ordinal-number>`에 의해 `artifactory.node.replicaCount`에서 정의된 복제본의 수에 따라 영구 볼륨 클레임을 생성, 예시 `volume-myrelease-artifactory-ha-member-0`와 `volume-myrelease-artifactory-ha-primary-1`.
- `helm install`와 `helm upgrade`에 파라미터 전달
```bash
...
--set artifactory.node.persistence.existingClaim=true
```

#### 기존 공유 볼륨 클레임

모든 노드에서 공유할 기존 클레임(데이터 및 백업)을 사용하려면:

- 명명 규칙과 일치하는 ReadWriteMany를 사용하여 PVC 생성:
```
  {{ template "artifactory-ha.fullname" . }}-data-pvc-<claim-ordinal>
  {{ template "artifactory-ha.fullname" . }}-backup-pvc-<claim-ordinal>
```
사용할 기존 클레임 2개를 보여 주는 예시:
```
  myexample-artifactory-ha-data-pvc-0
  myexample-artifactory-ha-backup-pvc-0
  myexample-artifactory-ha-data-pvc-1
  myexample-artifactory-ha-backup-pvc-1
```
- values.yaml 에서 artifactory.persistence.file-system.existingSharedClaim.enabled 를 true로 설정:
```
-- set artifactory.persistence.fileSystem.existingSharedClaim.enabled=true
```

#### NFS
NFS 서버를 클러스터의 스토리지로 사용하려면,
- NFS 서버 설정. `NFS_IP`로 IP 가져오기.
- 모든 사용자에게 쓰기 권한이 있는 NFS exported 디렉토리에 `data` 및 `backup` 디렉토리 생성
- NFS 파라미터를 `helm install` 및 `helm upgrade`에 전달
```bash
...
--set artifactory.persistence.type=nfs \
--set artifactory.persistence.nfs.ip=${NFS_IP} \
...
```

#### Google 스토리지
클러스터의 파일 저장소로 Google 스토리지 버킷을 사용하려면 다음과 같이 하십시오. [Google Storage Binary Provider](https://www.jfrog.com/confluence/display/RTF/Configuring+the+Filestore#ConfiguringtheFilestore-GoogleStorageBinaryProvider)를 참조하십시오.
- Google 스토리지 파라미터를 `helm install` 및 `helm upgrade`에 전달하십시오.
```bash
...
--set artifactory.persistence.type=google-storage \
--set artifactory.persistence.googleStorage.identity=${GCP_ID} \
--set artifactory.persistence.googleStorage.credential=${GCP_KEY} \
...
```

#### AWS S3
AWS S3 버킷을 클러스터의 파일 저장소로 사용하려면 다음과 같이 하십시오. [S3 Binary Provider](https://www.jfrog.com/confluence/display/RTF/Configuring+the+Filestore#ConfiguringtheFilestore-S3BinaryProvider)를 참조하십시오.
- AWS S3 파라미터를 `helm install` 및 `helm upgrade`에 전달하십시오.
```bash
...
# 명시적 자격 증명 사용:
--set artifactory.persistence.type=aws-s3 \
--set artifactory.persistence.awsS3.endpoint=${AWS_S3_ENDPOINT} \
--set artifactory.persistence.awsS3.region=${AWS_REGION} \
--set artifactory.persistence.awsS3.identity=${AWS_ACCESS_KEY_ID} \
--set artifactory.persistence.awsS3.credential=${AWS_SECRET_ACCESS_KEY} \
...

...
# 기존 IAM role 사용
--set artifactory.persistence.type=aws-s3 \
--set artifactory.persistence.awsS3.endpoint=${AWS_S3_ENDPOINT} \
--set artifactory.persistence.awsS3.region=${AWS_REGION} \
--set artifactory.persistence.awsS3.roleName=${AWS_ROLE_NAME} \
...
```
**NOTE:** S3 `endpoint`와 `region`이 일치하는지 확인하십시오. [AWS documentation on endpoint](https://docs.aws.amazon.com/general/latest/gr/rande.html)를 참조하십시오.

#### Microsoft Azure Blob 스토리지
Azure Blob Storage를 클러스터의 파일 저장소로 사용하려면 다음과 같이 하십시오. [Azure Blob Storage Binary Provider](https://www.jfrog.com/confluence/display/RTF/Configuring+the+Filestore#ConfiguringtheFilestore-AzureBlobStorageClusterBinaryProvider)를 참조하십시오.
- Azure Blob 파라미터를 `helm install` 및 `helm upgrade`에 전달하십시오.
```bash
...
--set artifactory.persistence.type=azure-blob \
--set artifactory.persistence.azureBlob.accountName=${AZURE_ACCOUNT_NAME} \
--set artifactory.persistence.azureBlob.accountKey=${AZURE_ACCOUNT_KEY} \
--set artifactory.persistence.azureBlob.endpoint=${AZURE_ENDPOINT} \
--set artifactory.persistence.azureBlob.containerName=${AZURE_CONTAINER_NAME} \
...
```

### 고유한 마스터 키 생성
Artifactory HA 클러스터는 고유한 마스터 키가 필요하다. 기본적으로 차트는 values.yaml (`artifactory.masterKey`)에 하나의 set를 가지고 있다.

**이 키는 데모용이며 프로덕션 환경에서 사용해서는 안 된다!**

고유한 템플릿을 생성하여 설치/업그레이드 시 템플릿에 전달하십시오.
```bash
# 키 생성
export MASTER_KEY=$(openssl rand -hex 32)
echo ${MASTER_KEY}

# 생성된 마스터 키를 헬름에 전달
helm install --name artifactory-ha --set artifactory.masterKey=${MASTER_KEY} jfrog/artifactory-ha
```

또는 마스터 키가 포함된 secret을 수동으로 생성하여 설치/업그레이드 시 템플릿에 전달할 수 있다.
```bash
# 키를 생성한다.
export MASTER_KEY=$(openssl rand -hex 32)
echo ${MASTER_KEY}

# 키를 포함한 secret 생성한다. secret의 키는 반드시 master-key라고 명명되어야 한다.
kubectl create secret generic my-secret --from-literal=master-key=${MASTER_KEY}

# 생성된 secret을 헬름에 전달한다.
helm install --name artifactory-ha --set artifactory.masterKeySecretName=my-secret jfrog/artifactory-ha
```앞으로의 모든 call에 대해 `helm install` 및 `helm upgrade`에 동일한 마스터 키를 전달한다.
어느 경우든, 미래의 모든 통신에 대해 동일한 마스터 키를 `helm install`와 `helm upgrade`에게 전달하십시오! 첫 번째 경우, 이는 항상 `--set artifactory.masterKey=${MASTER_KEY}`를 전달함을 의미한다.
**NOTE:** 둘 중 어느 케이스에도, 앞으로의 모든 call에 대해 반드시 동일한 마스터 키를 `helm install` 및 `helm upgrade`에  전달하십시오. 첫 번째 경우, `--set artifactory.masterKey=${MASTER_KEY}`. 둘째로, 이것은 항상 `--set artifactory.masterKeySecretName=my-secret`를 지나가고 secret의 내용이 변하지 않는 것을 보장한다는 것을 의미한다.

### Artifactory HA license 설치
Artifactory HA를 활성화하려면 적절한 라이센스를 설치해야 한다. 라이센스 관리에는 두 가지 방법이 있다. **Artifactory UI** 또는 **Kubernetes Secret**.

보다 쉽고 권장되는 방법은 **Artifactory UI**이다. **Kubernetes Secret**를 사용하는 것은 고급 사용자를 위한 것이며 자동화에 더 적합하다.

**IMPORTANT:** 다음 방법 중 하나만 사용해야 한다.
클러스터가 실행 중인 동안 이러한 클러스터 간에 전환하면 Artifactory HA 클러스터가 비활성화될 수 있음!

##### Artifactory UI
기본 클러스터가 실행되고 있으면 아티팩토리 UI를 열고 UI에 라이센스를 삽입하십시오. 자세한 내용은 [HA installation and setup](https://www.jfrog.com/confluence/display/RTF/HA+Installation+and+Setup)를 참조하십시오.

##### Kubernetes Secret
아티팩토리 라이센스를 [Kubernetes secret](https://kubernetes.io/docs/concepts/configuration/secret/)로 배포할 수 있다.
라이센스가 기록된 텍스트 파일을 준비하십시오. 여러 라이센스를 작성하는 경우(동일한 파일에 있어야 함), **각 라이센스 블록 사이에 두 개의 새 줄**을 배치하는 것이 중요함!
```bash
# Kubernetes secret 만들기 (로컬 라이센스 파일이 'art.lic'인 경우)
kubectl create secret generic artifactory-cluster-license --from-file=./art.lic

# 라이센스를 헬름에 전달
helm install --name artifactory-ha --set artifactory.license.secret=artifactory-cluster-license,artifactory.license.dataKey=art.lic jfrog/artifactory-ha
```
**NOTE:** 이 방법은 초기 배치에만 관련됨! 아티팩토리가 배포되면 라이센스가 이미 아티팩토리의 저장소에 유지되므로 이러한 매개 변수를 계속 전달하면 안 된다.
라이센스 업데이트는 아티팩토리 UI 또는 REST API를 통해 수행해야 한다.

##### 헬름 릴리즈의 일부로 secret 만들기
values.yaml
```yaml
artifactory:
  license:
    licenseKey: |-
      <LICENSE_KEY1>
      
      
      <LICENSE_KEY2>
      
      
      <LICENSE_KEY3>
```

```bash
helm install --name artifactory-ha -f values.yaml jfrog/artifactory-ha
```
**NOTE:** 이 방법은 초기 배치에만 관련됨! 아티팩토리가 배포되면 라이센스가 이미 아티팩토리의 저장소에 유지되므로 이러한 매개 변수를 계속 전달하면 안 된다.
라이센스 업데이트는 아티팩토리 UI 또는 REST API를 통해 수행해야 한다.
동일한 방법으로 아티팩트 라이센스를 계속 관리하려면, copyOnEveryStartup example을 values.yaml 파일에 표시

### NetworkPolicy 설정

NetworkPolicy는 이 네임스페이스에서 허용되는 ingress 및 egress 항목을 지정한다. 시스템 보안을 강화하기 위해 가능하면 더 구체화하도록 권장한다.

values.yaml의 `networkpolicy` 섹션에서 NetworkPolicy 객체 목록을 지정할 수 있다.

podSelector, ingress 및 egress의 경우 아무것도 제공되지 않으면 모든 것을 허용하는 기본 `- {}`이(가) 적용된다.

2개의 NetworkPolicy 개체를 생성하는 전체(그러나 매우 넓게 오픈된) example:
```
networkpolicy:
  # Allows all ingress and egress to/from artifactory primary and member pods.
  - name: artifactory
    podSelector:
      matchLabels:
        app: artifactory-ha
    egress:
    - {}
    ingress:
    - {}
  # Allows connectivity from artifactory-ha pods to postgresql pods, but no traffic leaving postgresql pod.
  - name: postgres
    podSelector:
      matchLabels:
        app: postgresql
    ingress:
    - from:
      - podSelector:
          matchLabels:
            app: artifactory-ha
```

### Bootstrapping Artifactory
**IMPORTANT:** 부트스트래핑 아티팩토리에는 라이선스가 필요하다. 위의 섹션에 표시된 대로 라이센스를 전달하십시오.

* [bootstrap Artifactory Global Configuration](https://www.jfrog.com/confluence/display/RTF/Configuration+Files#ConfigurationFiles-BootstrappingtheGlobalConfiguration)에 대한 사용자 안내서
* [bootstrap Artifactory Security Configuration](https://www.jfrog.com/confluence/display/RTF/Configuration+Files#ConfigurationFiles-BootstrappingtheSecurityConfiguration)에 대한 사용자 안내서

아래 그림과 같이 artifactory.config.import.xml과 security.import.xml이 있는 `bootstrap-config.yaml`를 만드십시오:
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-release-bootstrap-config
data:
  artifactory.config.import.xml: |
    <config contents>
  security.import.xml: |
    <config contents>
```

쿠버네티스에서 configMap 생성:
```bash
kubectl apply -f bootstrap-config.yaml
```

#### configMap을 헬름에 전달
```bash
helm install --name artifactory-ha --set artifactory.license.secret=artifactory-cluster-license,artifactory.license.dataKey=art.lic,artifactory.configMapName=my-release-bootstrap-config jfrog/artifactory-ha
```

### Nginx와 함께 사용자 지정 nginx.conf 사용

nginx.conf를 사용하여 configMap 생성 단계
* `nginx.conf` file 생성.
```bash
kubectl create configmap nginx-config --from-file=nginx.conf
```
* helm install에 configMap 전달
```bash
helm install --name artifactory-ha --set nginx.customConfigMap=nginx-config jfrog/artifactory-ha
```

### Artifactory 클러스터 스케일링
Artifactory HA의 주요 기능은 `--set artifactory.node.replicaCount=${CLUSTER_SIZE}`로 초기 클러스터 크기를 설정하고 필요한 경우 크기를 조정할 수 있는 기능이다.

##### 스케일링 전에
**IMPORTANT:** 스케일링할 때 데이터베이스 암호가 자동 생성 암호인 경우 암호를 명시적으로 전달해야 한다(이것은 동봉된 PostgreSQL helm chart의 디폴트).

현재 데이터베이스 암호 가져오기
```bash
export DB_PASSWORD=$(kubectl get $(kubectl get secret -o name | grep postgresql) -o jsonpath="{.data.postgres-password}" | base64 --decode)
```
모든 스케일 작업과 함께 `--set postgresql.postgresPassword=${DB_PASSWORD}`를 사용하여 설정된 클러스터 누락 방지!

##### 스케일 업
**2** 멤버 노드가 있는 클러스터가 있고 최대 **3** 멤버 노드(총 4개 노드)까지 확장하려는 경우를 가정해 보십시오.
```bash
# 4개 노드로 확장(기본 노드 1개 및 멤버 노드 3개)
helm upgrade --install artifactory-ha --set artifactory.node.replicaCount=3 --set postgresql.postgresPassword=${DB_PASSWORD} jfrog/artifactory-ha
```

##### 스케일 다운
**3*** 멤버 노드가 있는 클러스터를 **2** 멤버 노드로 축소한다고 가정해 보십시오.

```bash
# 멤버 노드 2개로 축소
helm upgrade --install artifactory-ha --set artifactory.node.replicaCount=2 --set postgresql.postgresPassword=${DB_PASSWORD} jfrog/artifactory-ha
```
- **NOTE:** 아티팩토리가 쿠버넷스 스테이트풀 세트로 실행되고 있기 때문에, 노드를 제거한다고 해서 영구적인 볼륨이 **제거되지는 않는다**. 명시적으로 제거해야 함
```bash
# PVC 리스트
kubectl get pvc

# 최고 서수가 있는 PVC를 제거하십시오!
# 이 예에서 가장 높은 노드 서수명은 2였으므로 스토리지를 제거해야 한다.
kubectl delete pvc volume-artifactory-node-2
```

### 외부 데이터베이스 사용
동봉된 **PostgreSQL**가 아닌 다른 데이터베이스를 사용하고자 하는 경우가 있다.
자세한 내용은 [configuring the database](https://www.jfrog.com/confluence/display/RTF/Configuring+the+Database) 참고
> 공식적인 아티팩토리 도커 이미지는 PostgreSQL database driver를 포함한다.
> 다른 데이터베이스 유형의 경우 Artifactory의 Tomcat/lib에 관련 데이터베이스 드라이버를 추가하십시오.

이 작업은 다음 매개 변수를 사용하여 수행할 수 있다.
```bash
# Artifactory Docker 이미지에 MySQL 데이터베이스 드라이버가 있는지 확인하십시오.
...
--set postgresql.enabled=false \
--set artifactory.preStartCommand="wget -O /opt/jfrog/artifactory/tomcat/lib/mysql-connector-java-5.1.41.jar https://jcenter.bintray.com/mysql/mysql-connector-java/5.1.41/mysql-connector-java-5.1.41.jar" \
--set database.type=mysql \
--set database.host=${DB_HOST} \
--set database.port=${DB_PORT} \
--set database.user=${DB_USER} \
--set database.password=${DB_PASSWORD} \
...
```
**NOTE:** 차트가 `database.*` 파라미터를 사용하려면 `postgresql.enabled=false`를 설정해야 한다. 그게 없다면, 그들은 무시될 것이다!

데이터베이스 자격 증명을 기존 쿠버네티스 `Secret`에 저장하면 `database.user`와 `database.password` 대신 `database.secrets`을 통해 지정할 수 있다:
```bash
# 데이터베이스 자격 증명을 포함하는 secret 만들기
kubectl create secret generic my-secret --from-literal=user=${DB_USER} --from-literal=password=${DB_PASSWORD}
...
--set postgresql.enabled=false \
--set database.secrets.user.name=my-secret \
--set database.secrets.user.key=user \
--set database.secrets.password.name=my-secret \
--set database.secrets.password.key=password \
...
```

### 아티팩토리 삭제
Artifactory HA 클러스터를 삭제하려면 다음과 같이 하십시오.
```bash
helm delete --purge artifactory-ha
```
이렇게 하면 Artifactory HA 클러스터가 완전히 삭제된다.
**NOTE:** 아티팩토리가 쿠버네티스 Stateful Sets로 실행되고 있으므로, 헬름 릴리즈를 제거해도 영구 볼륨은 제거되지 않는다. 명시적으로 제거해야 함
```bash
kubectl delete pvc -l release=artifactory-ha
```
자세한 내용은 [Kubernetes Stateful Set removal page](https://kubernetes.io/docs/tasks/run-application/delete-stateful-set/)를 참고

### 이미지에 대한 사용자 지정 도커 레지스트리
개인 레지스트리에서 도커 이미지를 뽑아야 할 경우(예: MySQL 데이터베이스 드라이버가 있는 사용자 지정 이미지가 있는 경우) [Kubernetes Docker registry secret](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)를 만들어 헬름에 전달해야 한다.
```bash
# 'regecret'이라는 Docker 레지스트리 secret 생성
kubectl create secret docker-registry regsecret --docker-server=${DOCKER_REGISTRY} --docker-username=${DOCKER_USER} --docker-password=${DOCKER_PASS} --docker-email=${DOCKER_EMAIL}
```
일단 생성되면 `helm`에게 전달한다.
```bash
helm install --name artifactory-ha --set imagePullSecrets=regsecret jfrog/artifactory-ha
```

### Logger 사이드카
이 차트는 아티팩토리의 다양한 로그에 사이드카를 추가하는 옵션을 제공한다. [values.yaml](values.yaml)에서 사용 가능한 값을 참조하십시오.

파드에서 컨테이너 리스트 조회
```bash
kubectl get pods -n <NAMESPACE> <POD_NAME> -o jsonpath='{.spec.containers[*].name}' | tr ' ' '\n'
```

특정 로그 조회
```bash
kubectl logs -n <NAMESPACE> <POD_NAME> -c <LOG_CONTAINER_NAME>
```


### 사용자 정의 초기화 컨테이너
메인 컨테이너를 구동시키기 전에 파일 시스템에서 어떤 것을 확인하거나 어떤 것을 테스트하는 것과 같은 특별하고 지원되지 않는 초기화 프로세스가 필요한 경우가 있다.

이를 위해 [values.yaml](values.yaml)에 사용자 정의 초기화 컨테이너를 작성하기 위한 섹션이 있다. 기본적으로 그것은 언급되어 있다.
```
artifactory:
  ## Add custom init containers
  customInitContainers: |
    ## Init containers template goes here ##
```

### 사용자 정의 사이드카 컨테이너
사이드카 컨테이너가 추가로 필요한 경우도 있다. 예를 들어 모니터링 에이전트 또는 로그 수집.

이를 위해 [values.yaml](values.yaml)에 사용자 정의 사이드카 컨테이너를 작성하기 위한 섹션이 있다. 기본적으로 그것은 언급되어 있다.
```
artifactory:
  ## Add custom sidecar containers
  customSidecarContainers: |
    ## Sidecar containers template goes here ##
```

컨테이너 템플릿에서 다음을 설정하여 필요에 따라 사용자 지정 사용자로 실행할 사이드카를 구성할 수 있음.
```
  # Example of running container as root (id 0)
  securityContext:
    runAsUser: 0
    fsGroup: 0
```

### 사용자 지정 볼륨
사용자 정의 초기화 또는 사이드카 컨테이너에서 사용자 정의 볼륨을 사용해야 하는 경우 이 옵션을 사용하십시오.

이를 위해 [values.yaml](values.yaml)에 사용자 지정 볼륨을 정의하는 섹션이 있다. 기본적으로 그것은 언급되어 있다.
```
artifactory:
  ## Add custom volumes
  customVolumes: |
    ## Custom volume comes here ##
```

### 설치하는 동안 아티팩토리 사용자 플러그인 추가
[Artifactory User Plugin](https://github.com/jfrog/artifactory-user-plugins)를 추가해야 한다면 이 옵션을 사용할 수 있다.

명령에 따라 [Artifactory User Plugin](https://github.com/jfrog/artifactory-user-plugins)로 비밀 만들기
```
# single user plugin이 포함된 secret
kubectl  create secret generic archive-old-artifacts --from-file=archiveOldArtifacts.groovy --namespace=artifactory-ha 

# 구성 파일이 있는 single user plugin이 포함된 secret
kubectl  create secret generic webhook --from-file=webhook.groovy  --from-file=webhook.config.json.sample --namespace=artifactory-ha
```

다음과 같이 플러그인 비밀명을 `plugins.yaml`에 추가하십시오.:
```yaml
artifactory:
  userPluginSecrets:
    - archive-old-artifacts
    - webhook
```

이제 생성된 `plugins.yaml` 파일을 전달하여 설치 명령을 조정하여 다음과 같이 사용자 플러그인을 사용하여 Artifactory를 배포하십시오:
```
helm install --name artifactory-ha -f plugins.yaml jfrog/artifactory-ha
```

또는 이 차트에 따라 달라지는 Helm 차트에서 secret을 만들고 싶은 상황에 처할 수 있다. 이 시나리오에서 secret의 이름은 템플릿 기능을 통해 동적으로 생성될 가능성이 있으므로 정적으로 명명된 secret의 전달은 불가능하다. 이 경우 차트는 [`tpl`](https://helm.sh/docs/charts_tips_and_tricks/#using-the-tpl-function) 함수를 통해 문자열을 템플릿으로 평가하도록 지원한다. - 차트의 `values.yaml` 파일에 다음을 추가하여 자신의 secret을 value이라고 이름 짓는 데 사용되는 템플릿 언어를 포함하는 raw 문자열을 전달하십시오:
```yaml
artifactory-ha: # Name of the artifactory-ha dependency
  artifactory:
    userPluginSecrets:
      - '{{ template "my-chart.fullname" . }}'
```
NOTE: userPluginSecrets를 정의함으로써 /tmp/plugins에 저장된 컨테이너 이미지에서 미리 정의된 플러그인을 재정의한다. 이때 [artifactory-pro:6.9.0](https://bintray.com/jfrog/artifactory-pro)는 `internalUser.groovy` 플러그인과 함께 분배된다. 사용자 플러그인 외에 이 플러그인이 필요한 경우 이러한 추가 플러그인을 userPluginSecrets의 일부로 포함해야 한다.

## Configuration
다음 표에는 아티팩트 차트의 구성 가능한 파라미터와 기본값이 나열되어 있다.

|         Parameter            |           Description             |                         Default                       |
|------------------------------|-----------------------------------|-------------------------------------------------------|
| `imagePullSecrets`           | Docker registry pull secret       |                                                       |
| `serviceAccount.create`   | Specifies whether a ServiceAccount should be created | `true`                                |
| `serviceAccount.name`     | The name of the ServiceAccount to create             | Generated using the fullname template |
| `rbac.create`             | Specifies whether RBAC resources should be created   | `true`                                |
| `rbac.role.rules`         | Rules to create                   | `[]`                                                     |
| `logger.image.repository` | repository for logger image       | `busybox`                                                |
| `logger.image.tag`        | tag for logger image              | `1.30`                                                   |
| `artifactory.name`                   | Artifactory name                     | `artifactory`                              |
| `artifactory.image.pullPolicy`       | Container pull policy                | `IfNotPresent`                             |
| `artifactory.image.repository`       | Container image                      | `docker.bintray.io/jfrog/artifactory-pro`  |
| `artifactory.image.version`          | Container image tag                  | `.Chart.AppVersion`                        |
| `artifactory.loggers`             | Artifactory loggers (see values.yaml for possible values) | `[]`                     |
| `artifactory.catalinaLoggers`     | Artifactory Tomcat loggers (see values.yaml for possible values) | `[]`              |
| `artifactory.customInitContainers`| Custom init containers                  |                                            |
| `artifactory.customSidecarContainers`| Custom sidecar containers            |                                            |
| `artifactory.customVolumes`       | Custom volumes                    |                                                  |
| `artifactory.customVolumeMounts`  | Custom Artifactory volumeMounts   |                                                  |
| `artifactory.userPluginSecrets`   | Array of secret names for Artifactory user plugins |                                 |
| `artifactory.masterKey`           | Artifactory Master Key. Can be generated with `openssl rand -hex 32` |`FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF`|
| `artifactory.masterKeySecretName` | Artifactory Master Key secret name                     |                             |
| `artifactory.accessAdmin.password`               | Artifactory access-admin password to be set upon startup|             |
| `artifactory.accessAdmin.secret`                 | Artifactory access-admin secret name |                                |
| `artifactory.accessAdmin.dataKey`                | Artifactory access-admin secret data key |                            |
| `artifactory.preStartCommand`                    | Command to run before entrypoint starts |                             |
| `artifactory.postStartCommand`                   | Command to run after container starts   |                             |
| `artifactory.license.licenseKey` | Artifactory license key. Providing the license key as a parameter will cause a secret containing the license key to be created as part of the release. Use either this setting or the license.secret and license.dataKey. If you use both, the latter will be used.  |           |
| `artifactory.license.secret` | Artifactory license secret name              |                                            |
| `artifactory.license.dataKey`| Artifactory license secret data key          |                                            |
| `artifactory.service.name`   | Artifactory service name to be set in Nginx configuration | `artifactory`                 |
| `artifactory.service.type`   | Artifactory service type                                  | `ClusterIP`                   |
| `artifactory.service.pool`   | Artifactory instances to be in the load balancing pool. `members` or `all` | `members`    |
| `artifactory.externalPort`   | Artifactory service external port                         | `8081`                        |
| `artifactory.internalPort`   | Artifactory service internal port                         | `8081`                        |
| `artifactory.internalPortReplicator` | Replicator service internal port | `6061`   |
| `artifactory.externalPortReplicator` | Replicator service external port | `6061`   |
| `artifactory.extraEnvironmentVariables`          | Extra environment variables to pass to Artifactory. Supports evaluating strings as templates via the [`tpl`](https://helm.sh/docs/charts_tips_and_tricks/#using-the-tpl-function) function. See [documentation](https://www.jfrog.com/confluence/display/RTF/Installing+with+Docker#InstallingwithDocker-SupportedEnvironmentVariables) |   |
| `artifactory.livenessProbe.enabled`              | Enable liveness probe                     |  `true`                                               |
| `artifactory.livenessProbe.path`                 | liveness probe HTTP Get path              |  `/artifactory/webapp/#/login`                        |
| `artifactory.livenessProbe.initialDelaySeconds`  | Delay before liveness probe is initiated  | 180                                                   |
| `artifactory.livenessProbe.periodSeconds`        | How often to perform the probe            | 10                                                    |
| `artifactory.livenessProbe.timeoutSeconds`       | When the probe times out                  | 10                                                    |
| `artifactory.livenessProbe.successThreshold`     | Minimum consecutive successes for the probe to be considered successful after having failed. | 1  |
| `artifactory.livenessProbe.failureThreshold`     | Minimum consecutive failures for the probe to be considered failed after having succeeded.   | 10 |
| `artifactory.readinessProbe.enabled`              | would you like a readinessProbe to be enabled           |  `true`                                |
| `artifactory.readinessProbe.path`                      | readiness probe HTTP Get path                           |  `/artifactory/webapp/#/login`           |
| `artifactory.readinessProbe.initialDelaySeconds` | Delay before readiness probe is initiated | 60                                                    |
| `artifactory.readinessProbe.periodSeconds`       | How often to perform the probe            | 10                                                    |
| `artifactory.readinessProbe.timeoutSeconds`      | When the probe times out                  | 10                                                    |
| `artifactory.readinessProbe.successThreshold`    | Minimum consecutive successes for the probe to be considered successful after having failed. | 1  |
| `artifactory.readinessProbe.failureThreshold`    | Minimum consecutive failures for the probe to be considered failed after having succeeded.   | 10 |
| `artifactory.copyOnEveryStartup`     | List of files to copy on startup from source (which is absolute) to target (which is relative to ARTIFACTORY_HOME   |  |
| `artifactory.persistence.mountPath`    | Artifactory persistence volume mount path           | `"/var/opt/jfrog/artifactory"`  |
| `artifactory.persistence.enabled`      | Artifactory persistence volume enabled              | `true`                          |
| `artifactory.persistence.accessMode`   | Artifactory persistence volume access mode          | `ReadWriteOnce`                 |
| `artifactory.persistence.size`         | Artifactory persistence or local volume size        | `200Gi`                         |
| `artifactory.persistence.maxCacheSize` | Artifactory cache-fs provider maxCacheSize in bytes | `50000000000`                   |
| `artifactory.persistence.type`         | Artifactory HA storage type                         | `file-system`                   |
| `artifactory.persistence.redundancy`   | Artifactory HA storage redundancy                   | `3`                             |
| `artifactory.persistence.nfs.ip`            | NFS server IP                        |                                     |
| `artifactory.persistence.nfs.haDataMount`   | NFS data directory                   | `/data`                             |
| `artifactory.persistence.nfs.haBackupMount` | NFS backup directory                 | `/backup`                           |
| `artifactory.persistence.nfs.dataDir`       | HA data directory                    | `/var/opt/jfrog/artifactory-ha`     |
| `artifactory.persistence.nfs.backupDir`     | HA backup directory                  | `/var/opt/jfrog/artifactory-backup` |
| `artifactory.persistence.nfs.capacity`      | NFS PVC size                         | `200Gi`                             |
| `artifactory.persistence.eventual.numberOfThreads`  | Eventual number of threads   | `10`                                |
| `artifactory.persistence.googleStorage.bucketName`  | Google Storage bucket name          | `artifactory-ha`             |
| `artifactory.persistence.googleStorage.identity`    | Google Storage service account id   |                              |
| `artifactory.persistence.googleStorage.credential`  | Google Storage service account key  |                              |
| `artifactory.persistence.googleStorage.path`        | Google Storage path in bucket       | `artifactory-ha/filestore`   |
| `artifactory.persistence.googleStorage.bucketExists`| Google Storage bucket exists therefore does not need to be created.| `false`             |
| `artifactory.persistence.awsS3.bucketName`          | AWS S3 bucket name                     | `artifactory-ha`             |
| `artifactory.persistence.awsS3.endpoint`            | AWS S3 bucket endpoint                 | See https://docs.aws.amazon.com/general/latest/gr/rande.html |
| `artifactory.persistence.awsS3.region`              | AWS S3 bucket region                   |                              |
| `artifactory.persistence.awsS3.roleName`            | AWS S3 IAM role name                   |                             |
| `artifactory.persistence.awsS3.identity`            | AWS S3 AWS_ACCESS_KEY_ID               |                              |
| `artifactory.persistence.awsS3.credential`          | AWS S3 AWS_SECRET_ACCESS_KEY           |                              |
| `artifactory.persistence.awsS3.properties`          | AWS S3 additional properties           |                              |
| `artifactory.persistence.awsS3.path`                | AWS S3 path in bucket                  | `artifactory-ha/filestore`   |
| `artifactory.persistence.awsS3.refreshCredentials`  | AWS S3 renew credentials on expiration | `true` (When roleName is used, this parameter will be set to true) |
| `artifactory.persistence.awsS3.testConnection`      | AWS S3 test connection on start up     | `false`                      |
| `artifactory.persistence.awsS3.s3AwsVersion`        | AWS S3 signature version               | `AWS4-HMAC-SHA256`           |
| `artifactory.persistence.azureBlob.accountName`     | Azure Blob Storage account name        | ``                        |
| `artifactory.persistence.azureBlob.accountKey`      | Azure Blob Storage account key         | ``                        |
| `artifactory.persistence.azureBlob.endpoint`        | Azure Blob Storage endpoint            | ``                        |
| `artifactory.persistence.azureBlob.containerName`   | Azure Blob Storage container name      | ``                        |
| `artifactory.persistence.azureBlob.testConnection`  | Azure Blob Storage test connection     | `false`                   |
| `artifactory.persistence.fileStorage.existingSharedClaim` | Enable using an existing shared pvc | `false`                             |
| `artifactory.persistence.fileStorage.dataDir`             | HA data directory                   | `/var/opt/jfrog/artifactory/artifactory-data`     |
| `artifactory.persistence.fileStorage.backupDir`           | HA backup directory                 | `/var/opt/jfrog/artifactory-backup` |
| `artifactory.javaOpts.other`                        | Artifactory additional java options (for all nodes) |              |
| `artifactory.replicator.enabled`                    | Enable Artifactory Replicator          | `false`                   |
| `artifactory.replicator.publicUrl`              | Artifactory Replicator Public URL |                                    |
| `artifactory.primary.resources.requests.memory` | Artifactory primary node initial memory request  |                     |
| `artifactory.primary.resources.requests.cpu`    | Artifactory primary node initial cpu request     |                     |
| `artifactory.primary.resources.limits.memory`   | Artifactory primary node memory limit            |                     |
| `artifactory.primary.resources.limits.cpu`      | Artifactory primary node cpu limit               |                     |
| `artifactory.primary.javaOpts.xms`              | Artifactory primary node java Xms size           |                     |
| `artifactory.primary.javaOpts.xmx`              | Artifactory primary node java Xms size           |                     |
| `artifactory.primary.javaOpts.other`            | Artifactory primary node additional java options |                     |
| `artifactory.primary.persistence.existingClaim` | Whether to use an existing pvc for the primary node | `false`            |
| `artifactory.node.replicaCount`                 | Artifactory member node replica count            | `2`                 |
| `artifactory.node.minAvailable`                 | Artifactory member node min available count      | `1`                 |
| `artifactory.node.resources.requests.memory`    | Artifactory member node initial memory request   |                     |
| `artifactory.node.resources.requests.cpu`       | Artifactory member node initial cpu request      |                     |
| `artifactory.node.resources.limits.memory`      | Artifactory member node memory limit             |                     |
| `artifactory.node.resources.limits.cpu`         | Artifactory member node cpu limit                |                     |
| `artifactory.node.javaOpts.xms`                 | Artifactory member node java Xms size            |                     |
| `artifactory.node.javaOpts.xmx`                 | Artifactory member node java Xms size            |                     |
| `artifactory.node.javaOpts.other`               | Artifactory member node additional java options  |                     |
| `artifactory.node.persistence.existingClaim`    | Whether to use existing PVCs for the member nodes | `false`            |
| `artifactory.terminationGracePeriodSeconds`     | Termination grace period (seconds)               | `30s`               |
| `ingress.enabled`           | If true, Artifactory Ingress will be created | `false`                                     |
| `ingress.annotations`       | Artifactory Ingress annotations     | `{}`                                                 |
| `ingress.labels`       | Artifactory Ingress labels     | `{}`                                                           |
| `ingress.hosts`             | Artifactory Ingress hostnames       | `[]`                                                 |
| `ingress.path`              | Artifactory Ingress path            | `/`                                                  |
| `ingress.tls`               | Artifactory Ingress TLS configuration (YAML) | `[]`                                        |
| `ingress.defaultBackend.enabled` | If true, the default `backend` will be added using serviceName and servicePort | `true` |
| `ingress.annotations`       | Ingress annotations, which are written out if annotations section exists in values. Everything inside of the annotations section will appear verbatim inside the resulting manifest. See `Ingress annotations` section below for examples of how to leverage the annotations, specifically for how to enable docker authentication. |  |
| `nginx.enabled`             | Deploy nginx server                      | `true`                                          |
| `nginx.name`                | Nginx name                        | `nginx`                                                |
| `nginx.replicaCount`        | Nginx replica count               | `1`                                                    |
| `nginx.uid`                 | Nginx User Id                     | `104`                                                  |
| `nginx.gid`                 | Nginx Group Id                    | `107`                                                  |
| `nginx.image.repository`    | Container image                   | `docker.bintray.io/jfrog/nginx-artifactory-pro`        |
| `nginx.image.version`       | Container version                 | `.Chart.AppVersion`                                    |
| `nginx.image.pullPolicy`    | Container pull policy             | `IfNotPresent`                                         |
| `nginx.loggers`        | Artifactory loggers (see values.yaml for possible values) | `[]`                           |
| `nginx.service.type`        | Nginx service type                | `LoadBalancer`                                         |
| `nginx.service.loadBalancerSourceRanges`| Nginx service array of IP CIDR ranges to whitelist (only when service type is LoadBalancer) |        |
| `nginx.service.annotations` | Nginx service annotations           | `{}`                            |
| `nginx.service.externalTrafficPolicy`| Nginx service desires to route external traffic to node-local or cluster-wide endpoints. | `Cluster` |
| `nginx.loadBalancerIP`| Provide Static IP to configure with Nginx |                                 |
| `nginx.externalPortHttp` | Nginx service external port            | `80`                            |
| `nginx.internalPortHttp` | Nginx service internal port            | `80`                            |
| `nginx.externalPortHttps` | Nginx service external port           | `443`                           |
| `nginx.internalPortHttps` | Nginx service internal port           | `443`                           |
| `nginx.internalPortReplicator` | Replicator service internal port | `6061`                          |
| `nginx.externalPortReplicator` | Replicator service external port | `6061`                          |
| `nginx.livenessProbe.enabled`              | would you like a liveness Probe to be enabled          |  `true`                                  |
| `nginx.livenessProbe.path`                 | liveness probe HTTP Get path              |  `/artifactory/webapp/#/login`                    |
| `nginx.livenessProbe.initialDelaySeconds`  | Delay before liveness probe is initiated  | 100                                                   |
| `nginx.livenessProbe.periodSeconds`        | How often to perform the probe            | 10                                                    |
| `nginx.livenessProbe.timeoutSeconds`       | When the probe times out                  | 10                                                    |
| `nginx.livenessProbe.successThreshold`     | Minimum consecutive successes for the probe to be considered successful after having failed. | 1  |
| `nginx.livenessProbe.failureThreshold`     | Minimum consecutive failures for the probe to be considered failed after having succeeded.   | 10 |
| `nginx.readinessProbe.enabled`             | would you like a readinessProbe to be enabled           |  `true`                                 |
| `nginx.readinessProbe.path`                     | Readiness probe HTTP Get path                           |  `/artifactory/webapp/#/login` |
| `nginx.readinessProbe.initialDelaySeconds` | Delay before readiness probe is initiated | 60                                                    |
| `nginx.readinessProbe.periodSeconds`       | How often to perform the probe            | 10                                                    |
| `nginx.readinessProbe.timeoutSeconds`      | When the probe times out                  | 10                                                    |
| `nginx.readinessProbe.successThreshold`    | Minimum consecutive successes for the probe to be considered successful after having failed. | 1  |
| `nginx.readinessProbe.failureThreshold`    | Minimum consecutive failures for the probe to be considered failed after having succeeded.   | 10 |
| `nginx.tlsSecretName` |  SSL secret that will be used by the Nginx pod |                                                 |
| `nginx.env.ssl`                   | Nginx Environment enable ssl               | `true`                                  |
| `nginx.env.skipAutoConfigUpdate`  | Nginx Environment to disable auto configuration update | `false`                     |
| `nginx.customConfigMap`           | Nginx CustomeConfigMap name for `nginx.conf` | ` `                                   |
| `nginx.customArtifactoryConfigMap`| Nginx CustomeConfigMap name for `artifactory-ha.conf` | ` `                          |
| `nginx.resources.requests.memory` | Nginx initial memory request               | `250Mi`                                 |
| `nginx.resources.requests.cpu`    | Nginx initial cpu request                  | `100m`                                  |
| `nginx.resources.limits.memory`   | Nginx memory limit                         | `250Mi`                                 |
| `nginx.resources.limits.cpu`      | Nginx cpu limit                            | `500m`                                  |
| `nginx.persistence.mountPath` | Nginx persistence volume mount path | `"/var/opt/jfrog/nginx"`                           |
| `nginx.persistence.enabled` | Nginx persistence volume enabled. This is only available when the nginx.replicaCount is set to 1 | `false`                                                  |
| `nginx.persistence.accessMode` | Nginx persistence volume access mode | `ReadWriteOnce`                                  |
| `nginx.persistence.size` | Nginx persistence volume size | `5Gi`                                                         |
| `postgresql.enabled`              | Use enclosed PostgreSQL as database        | `true`                                  |
| `postgresql.imageTag`             | PostgreSQL version                         | `9.6.11`                                |
| `postgresql.postgresDatabase`     | PostgreSQL database name                   | `artifactory`                           |
| `postgresql.postgresUser`         | PostgreSQL database user                   | `artifactory`                           |
| `postgresql.postgresPassword`     | PostgreSQL database password               |                                         |
| `postgresql.persistence.enabled`  | PostgreSQL use persistent storage          | `true`                                  |
| `postgresql.persistence.size`     | PostgreSQL persistent storage size         | `50Gi`                                  |
| `postgresql.service.port`         | PostgreSQL database port                   | `5432`                                  |
| `postgresql.resources.requests.memory`    | PostgreSQL initial memory request  |                                         |
| `postgresql.resources.requests.cpu`       | PostgreSQL initial cpu request     |                                         |
| `postgresql.resources.limits.memory`      | PostgreSQL memory limit            |                                         |
| `postgresql.resources.limits.cpu`         | PostgreSQL cpu limit               |                                         |
| `database.type`                  | External database type (`postgresql`, `mysql`, `oracle` or `mssql`)  |                       |
| `database.host`                  | External database hostname                         |                                         |
| `database.port`                  | External database port                             |                                         |
| `database.url`                   | External database connection URL                   |                                         |
| `database.user`                  | External database username                         |                                         |
| `database.password`              | External database password                         |                                         |
| `database.secrets.user.name`     | External database username `Secret` name           |                                         |
| `database.secrets.user.key`      | External database username `Secret` key            |                                         |
| `database.secrets.password.name` | External database password `Secret` name           |                                         |
| `database.secrets.password.key`  | External database password `Secret` key            |                                         |
| `database.secrets.url.name     ` | External database url `Secret` name                |                                         |
| `database.secrets.url.key`       | External database url `Secret` key                 |                                         |
| `networkpolicy.name`             | Becomes part of the NetworkPolicy object name                                  | `artifactory`                           |
| `networkpolicy.podselector`      | Contains the YAML that specifies how to match pods. Usually using matchLabels. |                                         |
| `networkpolicy.ingress`          | YAML snippet containing to & from rules applied to incoming traffic            | `- {}` (open to all inbound traffic)    |
| `networkpolicy.egress`           | YAML snippet containing to & from rules applied to outgoing traffic            | `- {}` (open to all outbound traffic)   |

`--set key=value[,key=value]` 인수를 사용하여 `helm install`를 사용하여 각 매개변수를 지정하십시오.

### Ingress와 TLS
Helm이 호스트 이름을 사용하여 ingress 개체를 만들도록 하려면 다음 두 줄을 Helm 명령에 추가하십시오:
```
helm install --name artifactory-ha \
  --set ingress.enabled=true \
  --set ingress.hosts[0]="artifactory.company.com" \
  --set artifactory.service.type=NodePort \
  --set nginx.enabled=false \
  jfrog/artifactory-ha
```

클러스터에서 TLS 인증서 (e.g. [cert-manager](https://github.com/jetstack/cert-manager)) 자동 생성/회수를 허용하는 경우 해당 메커니즘에 대한 설명서를 참조하십시오.

TLS를 수동으로 구성하려면 먼저 보호할 주소에 대한 키 및 인증서 쌍을 생성/복구하십시오. 그런 다음 네임스페이스에서 TLS 암호를 생성하십시오:

```console
kubectl create secret tls artifactory-tls --cert=path/to/tls.cert --key=path/to/tls.key
```

사용자 정의 `values.yaml` 파일의 아티팩토리 수신 TLS 섹션에 원하는 호스트 이름과 함께 secret 이름 포함:

```
  ingress:
    ## If true, Artifactory Ingress will be created
    ##
    enabled: true

    ## Artifactory Ingress hostnames
    ## Must be provided if Ingress is enabled
    ##
    hosts:
      - artifactory.domain.com
    annotations:
      kubernetes.io/tls-acme: "true"
    ## Artifactory Ingress TLS configuration
    ## Secrets must be manually created in the namespace
    ##
    tls:
      - secretName: artifactory-tls
        hosts:
          - artifactory.domain.com
```

### Ingress 어노테이션

이 예에서는 특히 아티팩토리가 리포지토리 경로 방법을 사용하여 도커 레지스트리로 작동할 수 있도록 한다. 이 설정에 대한 자세한 내용은 [Artifactory as Docker Registry](https://www.jfrog.com/confluence/display/RTF/Getting+Started+with+Artifactory+as+a+Docker+Registry) 설명서를 참조하십시오.

```
ingress:
  enabled: true
  defaultBackend:
    enabled: false
  hosts:
    - myhost.example.com
  annotations:
    ingress.kubernetes.io/force-ssl-redirect: "true"
    ingress.kubernetes.io/proxy-body-size: "0"
    ingress.kubernetes.io/proxy-read-timeout: "600"
    ingress.kubernetes.io/proxy-send-timeout: "600"
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/configuration-snippet: |
      rewrite ^/(v2)/token /artifactory/api/docker/null/v2/token;
      rewrite ^/(v2)/([^\/]*)/(.*) /artifactory/api/docker/$2/$1/$3;
    nginx.ingress.kubernetes.io/proxy-body-size: "0"
  tls:
    - hosts:
      - "myhost.example.com"
```

## 유용한 링크
- https://www.jfrog.com/confluence/display/EP/Getting+Started
- https://www.jfrog.com/confluence/display/RTF/Installing+Artifactory
- https://www.jfrog.com/confluence/
