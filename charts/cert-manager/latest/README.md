## Chart 설치

추가 구성 방법에 대한 세부 정보를 포함한 전체 설치 지침
cert-manager의 기능성은 [getting started docs](https://cert-manager.readthedocs.io/en/latest/getting-started/)에서 찾을 수 있다.

릴리스 이름  `my-release`로 차트를 설치하려면 다음과 같이 하십시오.:

```console
$ helm install --name my-release stable/cert-manager
```

인증서 발행을 시작하려면 ClusterIssuer를 설정해야 함
또는 발급자 리소스(예: 'letsencrypt-staging' 발급자 생성).

다양한 유형의 발급자 및 구성 방법에 대한 자세한 정보
문서에서 찾을 수 있음:

https://cert-manager.readthedocs.io/en/latest/reference/issuers.html

자동으로 프로비저닝하도록 인증 관리자 구성 방법에 대한 정보
ingress 리소스 인증서, `ingress-shim` 보기
문서화:

https://cert-manager.readthedocs.io/en/latest/reference/ingress-shim.html

> **Tip**: `helm list`를 사용하여 모든 릴리즈 나열

## Chart 삭제

`my-release` 배포를 제거/삭제하려면 다음과 같이 하십시오.:

```console
$ helm delete my-release
```

이 명령은 차트와 관련된 모든 쿠버네티스 구성요소를 제거하고 릴리즈를 삭제한다.

## Configuration

다음 표에는 cert-manager 차트의 구성 가능한 매개변수와 기본값이 나열되어 있다.

| Parameter | Description | Default |
| --------- | ----------- | ------- |
| `image.repository` | Image repository | `quay.io/jetstack/cert-manager-controller` |
| `image.tag` | Image tag | `v0.5.2` |
| `image.pullPolicy` | Image pull policy | `IfNotPresent` |
| `replicaCount`  | Number of cert-manager replicas  | `1` |
| `createCustomResource` | Create CRD/TPR with this release | `true` |
| `clusterResourceNamespace` | Override the namespace used to store DNS provider credentials etc. for ClusterIssuer resources | Same namespace as cert-manager pod
| `leaderElection.Namespace` | Override the namespace used to store the ConfigMap for leader election | Same namespace as cert-manager pod
| `certificateResourceShortNames` | Custom aliases for Certificate CRD | `["cert", "certs"]` |
| `extraArgs` | Optional flags for cert-manager | `[]` |
| `extraEnv` | Optional environment variables for cert-manager | `[]` |
| `rbac.create` | If `true`, create and use RBAC resources | `true` |
| `serviceAccount.create` | If `true`, create a new service account | `true` |
| `serviceAccount.name` | Service account to be used. If not set and `serviceAccount.create` is `true`, a name is generated using the fullname template |  |
| `resources` | CPU/memory resource requests/limits | |
| `nodeSelector` | Node labels for pod assignment | `{}` |
| `affinity` | Node affinity for pod assignment | `{}` |
| `tolerations` | Node tolerations for pod assignment | `[]` |
| `ingressShim.defaultIssuerName` | Optional default issuer to use for ingress resources |  |
| `ingressShim.defaultIssuerKind` | Optional default issuer kind to use for ingress resources |  |
| `ingressShim.defaultACMEChallengeType` | Optional default challenge type to use for ingresses using ACME issuers |  |
| `ingressShim.defaultACMEDNS01ChallengeProvider` | Optional default DNS01 challenge provider to use for ingresses using ACME issuers with DNS01 |  |
| `podAnnotations` | Annotations to add to the cert-manager pod | `{}` |
| `podDnsPolicy` | Optional cert-manager pod [DNS policy](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pods-dns-policy) |  |
| `podDnsConfig` | Optional cert-manager pod [DNS configurations](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pods-dns-config) |  |
| `podLabels` | Labels to add to the cert-manager pod | `{}` |
| `http_proxy` | Value of the `HTTP_PROXY` environment variable in the cert-manager pod | |
| `https_proxy` | Value of the `HTTPS_PROXY` environment variable in the cert-manager pod | |
| `no_proxy` | Value of the `NO_PROXY` environment variable in the cert-manager pod | |
| `webhook.enabled` | Toggles whether the validating webhook component should be installed | `false` |
| `webhook.replicaCount` | Number of cert-manager webhook replicas | `1` |
| `webhook.podAnnotations` | Annotations to add to the webhook pods | `{}` |
| `webhook.extraArgs` | Optional flags for cert-manager webhook component | `[]` |
| `webhook.resources` | CPU/memory resource requests/limits for the webhook pods | |
| `webhook.image.repository` | Webhook image repository | `quay.io/jetstack/cert-manager-webhook` |
| `webhook.image.tag` | Webhook image tag | `v0.5.2` |
| `webhook.image.pullPolicy` | Webhook image pull policy | `IfNotPresent` |

`helm install`에 대한 `--set key=value[,key=value]` 인수를 사용하여 각 매개변수를 지정하십시오.

또는 차트를 설치하는 동안 위 파라미터의 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```console
$ helm install --name my-release -f values.yaml .
```
> **Tip**: default [values.yaml](values.yaml)를 사용할 수 있다.

## 기여

이 차트는 [github.com/jetstack/cert-manager](https://github.com/jetstack/cert-manager/tree/master/contrib/charts/cert-manager)에서 유지된다.
