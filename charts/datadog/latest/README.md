### Datadog Cluster 에이전트 실행

[official documentation](https://docs.datadoghq.com/agent/kubernetes/cluster/)에서 Datadog Cluster 에이전트에 대해 읽어보십시오.

Datadog Cluster 에이전트를 사용하여 차트를 배포하려면 다음을 실행하십시오.
`clusterAgent.metricsProvider.enabled=true`를 지정하면 외부 메트릭 서버가 활성화된다는 점에 유의하십시오.
이 기능 사용법을 배우고 싶다면, 이 [walkthrough](https://github.com/DataDog/datadog-agent/blob/master/docs/cluster-agent/CUSTOM_METRICS_SERVER.md)를 확인해 보십시오.
리더 선택은 기본적으로 클러스터 에이전트의 차트에서 실행된다. 여러 개의 복제본이 구성된 경우(`clusterAgent.replicas` 사용) 클러스터 에이전트만 election에 참여하십시오.
클러스터 에이전트(들)q와 `clusterAgent.token`가 있는 에이전트 사이의 통신을 보호하는 데 사용되는 토큰을 지정할 수 있다. 지정하지 않으면 랜덤이 생성되고 차트를 설치할 때 경고 메시지가 표시된다.

```bash
helm install --name datadog-monitoring \
    --set datadog.apiKey=YOUR-API-KEY-HERE \
    --set datadog.appKey=YOUR-APP-KEY-HERE \
    --set clusterAgent.enabled=true \
    --set clusterAgent.metricsProvider.enabled=true \
    stable/datadog
```

## Chart 제거

`my-release` 배포를 제거/삭제하려면 다음과 같이 하십시오.:

```bash
helm delete my-release
```

이 명령은 차트와 관련된 모든 쿠버네티스 구성요소를 제거하고 릴리즈를 삭제한다.

## Configuration

다음 표에는 Datadog 차트의 구성 가능한 파라미터와 기본값이 나열되어 있다.

|             Parameter       |            Description             |                    Default                |
|-----------------------------|------------------------------------|-------------------------------------------|
| `datadog.apiKey`            | Your Datadog API key               |  `Nil` You must provide your own key      |
| `datadog.apiKeyExistingSecret` | If set, use the secret with a provided name instead of creating a new one |`nil` |
| `datadog.appKey`            | Datadog APP key required to use metricsProvider |  `Nil` You must provide your own key      |
| `datadog.appKeyExistingSecret` | If set, use the secret with a provided name instead of creating a new one |`nil` |
| `image.repository`          | The image repository to pull from  | `datadog/agent`                           |
| `image.tag`                 | The image tag to pull              | `6.9.0`                                   |
| `image.pullPolicy`          | Image pull policy                  | `IfNotPresent`                            |
| `image.pullSecrets`         | Image pull secrets                 |  `nil`                                    |
| `rbac.create`               | If true, create & use RBAC resources | `true`                                  |
| `rbac.serviceAccount`       | existing ServiceAccount to use (ignored if rbac.create=true) | `default`       |
| `datadog.name`              | Container name if Daemonset or Deployment | `datadog`                          |
| `datadog.site`              | Site ('datadoghq.com' or 'datadoghq.eu') | `nil`                                |
| `datadog.dd_url`            | Datadog intake server              | `nil`                                     |
| `datadog.env`               | Additional Datadog environment variables | `nil`                               |
| `datadog.logsEnabled`       | Enable log collection              | `nil`                                     |
| `datadog.logsConfigContainerCollectAll` | Collect logs from all containers | `nil`                           |
| `datadog.logsPointerHostPath` | Host path to store the log tailing state in | `/var/lib/datadog-agent/logs`   |
| `datadog.apmEnabled`        | Enable tracing from the host       | `nil`                                     |
| `datadog.processAgentEnabled` | Enable live process monitoring   | `nil`                                     |
| `datadog.checksd`           | Additional custom checks as python code  | `nil`                               |
| `datadog.confd`             | Additional check configurations (static and Autodiscovery) | `nil`             |
| `datadog.criSocketPath`     | Path to the container runtime socket (if different from Docker) | `nil`        |
| `datadog.tags`              | Set host tags                      | `nil`                                     |
| `datadog.nonLocalTraffic` | Enable statsd reporting from any external ip | `False`                           |
| `datadog.useCriSocketVolume` | Enable mounting the container runtime socket in Agent containers | `True` |
| `datadog.dogstatsdOriginDetection` | Enable origin detection for container tagging | `False`                 |
| `datadog.useDogStatsDSocketVolume` | Enable dogstatsd over Unix Domain Socket | `False`                      |
| `datadog.volumes`           | Additional volumes for the daemonset | `nil`                     |
| `datadog.volumeMounts`      | Additional volumeMounts for the daemonset | `nil`                |
| `datadog.podAnnotationsAsTags` | Kubernetes Annotations to Datadog Tags mapping | `nil`                      |
| `datadog.podLabelsAsTags`   | Kubernetes Labels to Datadog Tags mapping      | `nil`                         |
| `datadog.resources.requests.cpu` | CPU resource requests         | `200m`                                    |
| `datadog.resources.limits.cpu` | CPU resource limits             | `200m`                                    |
| `datadog.resources.requests.memory` | Memory resource requests   | `256Mi`                                   |
| `datadog.resources.limits.memory` | Memory resource limits       | `256Mi`                                   |
| `datadog.securityContext`   | Allows you to overwrite the default securityContext applied to the container  | `nil`  |
| `datadog.livenessProbe`     | Overrides the default liveness probe | http port 5555                          |
| `datadog.hostname`          | Set the hostname (write it in datadog.conf) | `nil`                            |
| `datadog.acInclude`         | Include containers based on image name | `nil`                                 |
| `datadog.acExclude`         | Exclude containers based on image name | `nil`                                 |
| `daemonset.podAnnotations`  | Annotations to add to the DaemonSet's Pods | `nil`                             |
| `daemonset.tolerations`     | List of node taints to tolerate (requires Kubernetes >= 1.6) | `nil`           |
| `daemonset.nodeSelector`    | Node selectors                     | `nil`                                     |
| `daemonset.affinity`        | Node affinities                    | `nil`                                     |
| `daemonset.useHostNetwork`  | If true, use the host's network    | `nil`                                     |
| `daemonset.useHostPID`.     | If true, use the host's PID namespace    | `nil`                               |
| `daemonset.useHostPort`     | If true, use the same ports for both host and container  | `nil`               |
| `daemonset.priorityClassName` | Which Priority Class to associate with the daemonset| `nil`                  |
| `datadog.leaderElection`    | Enable the leader Election feature | `false`                                   |
| `datadog.leaderLeaseDuration`| The duration for which a leader stays elected.| `nil`                         |
| `datadog.collectEvents`     | Enable Kubernetes event collection. Requires leader election. | `false`        |
| `kubeStateMetrics.enabled`  | If true, create kube-state-metrics | `true`                                    |
| `kube-state-metrics.rbac.create`| If true, create & use RBAC resources for kube-state-metrics | `true`       |
| `kube-state-metrics.rbac.serviceAccount` | existing ServiceAccount to use (ignored if rbac.create=true) for kube-state-metrics | `default` |
| `clusterAgent.enabled`                   | Use the cluster-agent for cluster metrics (Kubernetes 1.10+ only) | `false`                           |
| `clusterAgent.token`                     | A cluster-internal secret for agent-to-agent communication. Must be 32+ characters a-zA-Z | Generates a random value |
| `clusterAgent.containerName`             | The container name for the Cluster Agent  | `cluster-agent`                           |
| `clusterAgent.image.repository`          | The image repository for the cluster-agent | `datadog/cluster-agent`                           |
| `clusterAgent.image.tag`                 | The image tag to pull              | `1.0.0`                                   |
| `clusterAgent.image.pullPolicy`          | Image pull policy                  | `IfNotPresent`                            |
| `clusterAgent.image.pullSecrets`         | Image pull secrets                 |  `nil`                                    |
| `clusterAgent.metricsProvider.enabled`   | Enable Datadog metrics as a source for HPA scaling |  `false`                  |
| `clusterAgent.resources.requests.cpu`    | CPU resource requests              | `200m`                                    |
| `clusterAgent.resources.limits.cpu`      | CPU resource limits                | `200m`                                    |
| `clusterAgent.resources.requests.memory` | Memory resource requests           | `256Mi`                                   |
| `clusterAgent.resources.limits.memory`   | Memory resource limits             | `256Mi`                                   |
| `clusterAgent.tolerations`               | List of node taints to tolerate    | `[]`                                      |
| `clusterAgent.livenessProbe`             | Overrides the default liveness probe | http port 443 if external metrics enabled       |
| `clusterAgent.readinessProbe`            | Overrides the default readiness probe | http port 443 if external metrics enabled      |

`--set key=value[,key=value]` 인수를 사용하여 `helm install`를 사용하여 각 매개변수를 지정하십시오. 예를 들면,

```bash
helm install --name my-release \
  --set datadog.apiKey=YOUR-KEY-HERE,datadog.logLevel=DEBUG \
  stable/datadog
```

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
helm install --name my-release -f my-values.yaml stable/datadog
```

**Tip**: 기본 [values.yaml](values.yaml)를 복사하고 사용자 지정할 수 있음.

### Image repository 및 tag

Datadog [offers two variants](https://hub.docker.com/r/datadog/agent/tags/), JMX/java 통합을 실행해야 하는 경우 `-jmx` 태그로 전환하십시오. 이 도표는 또한 운영 중인 [the standalone dogstatsd image](https://hub.docker.com/r/datadog/dogstatsd/tags/)를 지원한다.

버전 1.0.0부터 이 차트는 에이전트 5.x 배포를 더 이상 지원하지 않는다. 에이전트 6.x로 업그레이드할 수 없는 경우 `--version 0.18.0`와 함께 헬름 설치를 호출하여 이전 버전의 차트를 사용할 수 있다.

### DaemonSet 및 Deployment

기본적으로 Datadog 에이전트는 DaemonSet에서 실행된다. 또는 특수 사용 사례에 대한 Deployment 내에서 실행될 수 있다.

**Note:** 단일 릴리스 내의 동시 DaemonSet + Deployment 설치는 이를 달성하기 위해 두 개의 릴리스가 필요한 향후 버전에서 더 이상 사용되지 않을 것이다.

### Secret

기본적으로 이 차트는 Secret을 생성하고 API 키를 해당 Secret에 넣는다.
그러나 `datadog.apiKeyExistingSecret` 값을 설정하여 수동으로 생성한 암호를 사용할 수 있다.

### confd 및 checksd

Datadog [entrypoint
](https://github.com/DataDog/datadog-agent/blob/master/Dockerfiles/agent/entrypoint/89-copy-customfiles.sh)는 `/conf.d`에서 발견되는 `.yaml` 확장자로 파일을 복사하고, `/check.d`에서 `/etc/datadog-agent/conf.d` 와 `/etc/datadog-agent/checks.d`까지 각각 `.py` 확장된 파일을 복사한다. `datadog.confd`와 `datadog.checksd`의 key들은 각각의 ConfigMaps, ie에서 발견되는 내용물을 반영해야 한다.

```yaml
datadog:
  confd:
    redisdb.yaml: |-
      ad_identifiers:
        - redis
        - bitnami/redis
      init_config:
      instances:
        - host: "%%host%%"
          port: "%%port%%"
    jmx.yaml: |-
      ad_identifiers:
        - openjdk
      instance_config:
      instances:
        - host: "%%host%%"
          port: "%%port_0%%"
    redisdb.yaml: |-
      init_config:
      instances:
        - host: "outside-k8s.example.com"
          port: 6379
```

자세한 내용은 [the documentation](https://docs.datadoghq.com/agent/kubernetes/integrations/)를 참조하십시오.

### 쿠버네티스 이벤트 컬렉션

이벤트 수집을 가능하게 하려면 `datadog.leaderElection`, `datadog.collectEvents` 및 `rbac.create` 옵션을 `true`으로 설정해야 한다.

이제 Datadog Cluster 에이전트를 사용하여 이벤트를 수집하는 것을 권장 - [Enabling the Datadog Cluster Agent](#enabling-the-datadog-cluster-agent) 부분을 참조하십시오.
자세한 내용은 [the official documentation](https://docs.datadoghq.com/agent/kubernetes/event_collection/)을 참조하십시오.

### 쿠버네티스 Labels 및 Annotations

Kubernetes pod lables 및 annotations를 Datadog 태그에 매핑하려면 Kubernetes labels/annotations를 키로, datadog 태그를 값으로 하는 dictionary를 제공하십시오:

```yaml
podAnnotationsAsTags:
  iam.amazonaws.com/role: kube_iamrole
```

```yaml
podLabelsAsTags:
  app: kube_app
  release: helm_release
```

### CRI 통합

버전 6.6.0을 기준으로 Datadog 에이전트는 클러스터에서 사용되는 모든 컨테이너 런타임 인터페이스에서 메트릭 수집을 지원한다.
`datadog.criSocketPath`로 소켓의 위치 경로를 구성하고 `datadog.useCriSocketVolume`를 `True`로 설정하여 에이전트를 실행하는 포드에 소켓이 장착되도록 하십시오.
표준 경로는:

- Containerd socket: `/var/run/containerd/containerd.sock`
- Cri-o socket: `/var/run/crio/crio.sock`
