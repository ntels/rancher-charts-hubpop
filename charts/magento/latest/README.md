## Configuration

다음 표에는 Magento 차트의 구성 가능한 파라미터와 기본값이 나열되어 있다.

|             Parameter              |               Description                |                         Default                          |
|------------------------------------|------------------------------------------|----------------------------------------------------------|
| `image.registry`                   | Magento image registry                   | `docker.io`                                              |
| `image.repository`                 | Magento Image name                       | `bitnami/magento`                                        |
| `image.tag`                        | Magento Image tag                        | `{VERSION}`                                              |
| `image.pullPolicy`                 | Image pull policy                        | `Always` if `imageTag` is `latest`, else `IfNotPresent`  |
| `image.pullSecrets`                | Specify image pull secrets               | `nil`                                                    |
| `magentoHost`                      | Magento host to create application URLs  | `nil`                                                    |
| `magentoLoadBalancerIP`            | `loadBalancerIP` for the magento Service | `nil`                                                    |
| `magentoUsername`                  | User of the application                  | `user`                                                   |
| `magentoPassword`                  | Application password                     | _random 10 character long alphanumeric string_           |
| `magentoEmail`                     | Admin email                              | `user@example.com`                                       |
| `magentoFirstName`                 | Magento Admin First Name                 | `FirstName`                                              |
| `magentoLastName`                  | Magento Admin Last Name                  | `LastName`                                               |
| `magentoMode`                      | Magento mode                             | `developer`                                              |
| `magentoAdminUri`                  | Magento prefix to access Magento Admin   | `admin`                                                  |
| `allowEmptyPassword`               | Allow DB blank passwords                 | `yes`                                                    |
| `externalDatabase.host`            | Host of the external database            | `nil`                                                    |
| `externalDatabase.port`            | Port of the external database            | `3306`                                                   |
| `externalDatabase.user`            | Existing username in the external db     | `bn_magento`                                             |
| `externalDatabase.password`        | Password for the above username          | `nil`                                                    |
| `externalDatabase.database`        | Name of the existing database            | `bitnami_magento`                                        |
| `mariadb.enabled`                  | Whether to use the MariaDB chart           | `true`                                                   |
| `mariadb.mariadbRootPassword`      | MariaDB admin password                   | `nil`                                                    |
| `mariadb.mariadbDatabase`          | Database name to create                  | `bitnami_magento`                                        |
| `mariadb.mariadbUser`              | Database user to create                  | `bn_magento`                                             |
| `mariadb.mariadbPassword`          | Password for the database                | _random 10 character long alphanumeric string_           |
| `serviceType`                      | Kubernetes Service type                  | `LoadBalancer`                                           |
| `persistence.enabled`              | Enable persistence using PVC             | `true`                                                   |
| `persistence.apache.storageClass`  | PVC Storage Class for Apache volume      | `nil`  (uses alpha storage annotation)                   |
| `persistence.apache.accessMode`    | PVC Access Mode for Apache volume        | `ReadWriteOnce`                                          |
| `persistence.apache.size`          | PVC Storage Request for Apache volume    | `1Gi`                                                    |
| `persistence.magento.storageClass` | PVC Storage Class for Magento volume     | `nil`  (uses alpha storage annotation)                   |
| `persistence.magento.accessMode`   | PVC Access Mode for Magento volume       | `ReadWriteOnce`                                          |
| `persistence.magento.size`         | PVC Storage Request for Magento volume   | `8Gi`                                                    |
| `resources`                        | CPU/Memory resource requests/limits      | Memory: `512Mi`, CPU: `300m`                             |

위의 파라미터는 [bitnami/magento](http://github.com/bitnami/bitnami-docker-magento)에 정의된 환경 변수에 매핑된다. 자세한 내용은 [bitnami/magento](http://github.com/bitnami/bitnami-docker-magento) 이미지 설명서를 참조하십시오.

> **Note**:
>
> Magento가 올바르게 작동하려면 `magentoHost` 매개 변수를 지정하여 FQDN(권장) 또는 Magento 서비스의 공용 IP 주소를 지정해야 한다.
>
> 선택적으로, 예약 IP 주소를 차트의 Magento 서비스에 할당할 `magentoLoadBalancerIP` 매개 변수를 지정할 수 있다. 그러나 이 기능은 일부 클라우드 공급자(예: GKE)에서만 사용할 수 있다는 점에 유의하십시오.
>
> GKE에서 공용 IP 주소를 예약하려면 다음과 같이 하십시오:
>
> ```bash
> $ gcloud compute addresses create magento-public-ip
> ```
>
> 예약된 IP 주소는 차트를 설치하는 동안 `magentoLoadBalancerIP` 매개 변수의 값으로 지정하여 Magento 서비스와 연결할 수 있다.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```console
$ helm install --name my-release \
  --set magentoUsername=admin,magentoPassword=password,mariadb.mariadbRootPassword=secretpassword \
    stable/magento
```

위 명령은 마젠토 관리자 계정 사용자 이름과 암호를 각각 `admin`과 `password`로 설정한다.
또한 마리아DB `root` 사용자 암호를 `secretpassword`로 설정한다.

또는 차트를 설치하는 동안 위 파라미터의 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들어,

```console
$ helm install --name my-release -f values.yaml stable/magento
```

> **Tip**: 기본값 [values.yaml](values.yaml)

## 영구적

[Bitnami Magento](https://github.com/bitnami/bitnami-docker-magento) 이미지는 Magento 데이터 및 구성을 컨테이너의 `/bitnami/magento` 및 `/bitnami/apache` 경로에 저장합니다.

영구 볼륨 클레임은 배포 전반에 걸쳐 데이터를 유지하는 데 사용됩니다. 이것은 GCE, AWS 및 minikube에서 작동하는 것으로 알려져 있습니다.
PVC를 구성하거나 지속성을 비활성화하려면 [Configuration](#configuration) 섹션을 참조하십시오.